#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 17 15:07:11 2018

@author: Sandra's Family Company
"""
'''
Introduction
-------------
In Ontario secondary schools, calculus and advanced function is two of the compulsory courses that need lots of graphing skills. 
When we solve problems using mathematical models, it sometimes takes plenty of time to calculate functions’ derivative and draw the graph. 
In this context, computers can solve the problems much faster, so our group members designed two programs for the matter. 
As for the first program, it has the ability to not only calculate derivative but also graph and find the critical points such as roots, local maximum and minimum and inflection points for polynomial functions. 
However, because of the complicated situation in math problems, it is not enough to only focus on one type of function. 
Thus, the second program is designed to find derivative for all type of functions. 


Business Plan
-------------
Those two programs are really helpful for all the secondary school students and teachers, so we want to sell our product for fifteen dollars per person. 
Imagine after you finish all your homework, using reliable programs to check your answer. It is a really good idea to help you study efficiently. 
Furthermore, please don’t worry if you can’t understand the code. 
In order to understand the program and use it easily, we used print statements and comments that provide enough instructions. 
During the process of designing, we meet lots of difficulties which need time to overcome.
Doing research and debuging cost plenty of time. Our effort is worth the price. 
Come and buy them and you will benefit a lot, the price will be 35% off for the first two weeks the program is released.
'''

#from __future__ import generators 

import numpy as np
import matplotlib.pyplot as plt
import mpl_toolkits.axisartist as axis

class Welcome():
    def __init__(self):
        print('''\nWelcome to your new favorite math tool!\n\nThe program is able to differentiate, evaluate and graph polynomial functions of any degree.\nPlease remember to use the correct input to the code so that the computer does not misunderstand it.\nFor example, do not write a variable instead of a coefficient\nHowever, the program can only graph and label a polynomial function.\nIf you own the program, feel free to modify it to your preferences\n\n''')

class Polynomial:
    #From the user's input, coefficients are in the form a_n, ...a_3,a_2,a_1, a_0 
    # storing coefficients in reverse order to improve efficiency i.e. a_0, a_1,a_2,a_3, ... a_n
    # tuple is converted into list
    def __init__(self, *coefficients):
        self.coefficients = coefficients[::-1] 

    #calculate the y-value for each x-value
    def __call__(self, x):    
        result = 0
        for index, coeff in enumerate(self.coefficients):
            result += coeff * x** index
        return result 
    
    #Find the derivative of the function
    def derivative(self):
        derived_coefficients = []
        power = 1
        for i in range(1, len(self.coefficients)):
            derived_coefficients.append(self.coefficients[i] * power)
            power += 1
        return Polynomial(*derived_coefficients[::-1])
    
C= Welcome()
#get the information(degree and coefficients) of polynomials from user
print("Please input the highest degree of your function:")
degree = int(input())
coefficient=[]
for j in range(0,degree+1,1):
    print("Please input the coefficient of x^",end="")
    print(degree-j,":")
    number=float(input())
    coefficient.append(number)#collect all the coefficients into a list


p=Polynomial(*coefficient)  #p is an instance of class Polynomial
p_der = p.derivative() #the derivative
p_sec = p_der.derivative()#the second derivative

#initialize the grid
plt.grid(True)
down=float(input("The minimum of y-value on y-axis:"))
up=float(input("The maximum of y-value on y-axis:"))
plt.ylim(down,up) #only show the y-values in the interval [down,up], which can zoom in the significant part of the graph
left=float(input("The minimum of x-value(left end point) on x-axis (>=-10):"))
right=float(input("The maximum of x-value(right end point) on x-axis (<=10):"))
plt.xlim(left,right) #only show the x-values in the interval [left,right], which can zoom in the significant part of the graph
X = np.linspace(-10, 10, 1000, endpoint=True) #choose 1000 points on the graph
F = p(X)
F_derivative = p_der(X)  # find the derivative of the polynomial
F_sec = p_sec(X) # find the second derivative of the polynomial
plt.plot(X, F, label="F") #draw the graph of the function

'''
#if you want, these codes below can draw the graph of first derivative and second derivative for you.
plt.plot(X, F_derivative, label="F'")
plt.plot(X, F_sec, label="F''")
#To make the graph neat, we save them as comments.
'''

rootx = []#create a list to store the roots found
locmaxx = []#create a list to store the x-value of local maximum point found
locminx = []#create a list to store the x-value of local minimum point found
infle = []#create a list to store the x-value of inflection points found
flag=1

#flag is set in case of the second derivative is always 0
for j in np.arange(-5,5,0.001):#set the interval of x where we find the function's roots, local value and inflection points.
    i=round(j,3)
    i1=round(j,3)
    i2=round(i1+0.001,3)#to test x-values in 3 decimal place.
    
    if (round(p(i1),10)*round(p(i2),10)<0 or p(i1)==0):
    #to find where the y value is 0 ->  y-values of left side and the right side of the root would have different sign./the product of them would be negative.
        if(round(i,2) not in rootx): #make sure we do not append the same root .
            rootx.append(round(i,2)) # the root is approximately i rounded to 2 decimals
    
    if (round(p_der(i1),10)*round(p_der(i2),10)<0 or p_der(i1)==0):# to find where the derivative is 0.(similar methods as finding the root)
        if p_sec(i)<0.0:#if first derivative = 0. second derivative <0 .it is local maximum.
            #print(i,p_sec(i))
            if(round(i,2) not in locmaxx): #make sure we do not append the same value .
                locmaxx.append(round(i,2)) # the x-value of local maximum point is approximately i rounded to 2 decimals
        if p_sec(i)>0.0:#if first derivative = 0. second derivative >0 .it is local minimum.
            if(round(i,2) not in locminx): #make sure we do not append the same value.
                locminx.append(round(i,2)) # the x-value of local minimum point is approximately i rounded to 2 decimals
   
    if (round(p_sec(i1),10)*round(p_sec(i2),10)<0 or p_sec(i1)==0): # to find where the second derivative is 0.(similar methods as finding the root)
        if(round(i,2) not in infle): #make sure we do not append the same value.
            infle.append(round(i,2)) # the x-value of inflection point is approximately i rounded to 2 decimals
    else: flag=0 
    #once there is a point where second derivative is not 0, flag becomes 0.
    #avoid the case where there is no reflection points, but if we don't set flag, x-values in whole domain would be recorded as inflection point.
    
print("==============================================")
#seperate the display parts so that it will be neat
#we will draw 3 graphs for root, local value and inflection points respectively.
if len(rootx) > 0: #to check whether the fuction has root in the interval (-5,5)
    print("root",rootx)
    for k in range(len(rootx)):
        plt.plot(rootx[k],p(k))#mark the roots 
        str='%.2f' % rootx[k]#change the type of root from integer to string so that it can be displayed in the graph
        plt.annotate(str,xy = (rootx[k],p(rootx[k])),ha='right',va='bottom',bbox=dict(boxstyle='round,pad=0.000000001',fc='c',alpha=0.5))
        #using its x-value to label the root in blue box.
else:
    print("no root")
    #if there is no root, we print no root.
    
#to draw the first graph
#the first graph includes the graph of the function and the roots if they exist
plt.plot()
plt.legend()
plt.show()

print("==============================================")
#seperate the display parts so that it will be neat
#prepare to draw the second graph
plt.grid(True)
#the graph can focus more on the central part.
plt.ylim(down,up) 
plt.xlim(left,right)
X = np.linspace(-10, 10, 1000, endpoint=True)
F = p(X)
plt.plot(X, F, label="F")

if len(locmaxx) > 0: #to check whether the fuction has local maximum in the interval (-5,5)
    print("x-value of local maximum point",locmaxx)
    for k in range(len(locmaxx)):
        plt.plot(locmaxx[k],p_der(i))#mark the local maximum
        plt.annotate('%.2f' % locmaxx[k],xy = (locmaxx[k],p(locmaxx[k])),ha='right',va='bottom',bbox=dict(boxstyle='round,pad=0.00000001',fc='orange',alpha=0.5))
        #use its x-value to label the local maximum in orange box.
else:
    print("No local maximum")
    
if len(locminx) > 0: #to check whether the fuction has local minimum in the interval (-5,5)
    print("x-value of local minimum point",locminx)
    for k in range(len(locminx)):
        plt.plot(locminx[k],p_der(i))#mark the local minimum
        plt.annotate('%.2f' % locminx[k],xy = (locminx[k],p(locminx[k])),ha='right',va='bottom',bbox=dict(boxstyle='round,pad=0.0000001',fc='orange',alpha=0.5))
        #using its x-value label the local minimum in orange box.
else:
    print("No local minimum")
if len(locmaxx) == 0 and len(locminx) == 0:
    plt.annotate("No local value in this range",xy = (5,50),textcoords='offset points',ha='right',va='bottom',bbox=dict(boxstyle='round,pad=0.000001',fc='orange',alpha=0.5))
#if there is no local maximum/minimum, we tell the user by print statement.

#draw the second graph
#the second graph includes the graph of the function, the local maximum points and the local minimums point if they exist
plt.plot()
plt.legend()
plt.show()

print("==============================================")
#seperate the display parts so that it will be neat
#prepare to draw the third graph.
plt.grid(True)
#the graph can focus more on the central part.
plt.ylim(down,up)
plt.xlim(left,right)
X = np.linspace(-10, 10, 1000, endpoint=True)
F = p(X)
plt.plot(X, F, label="F")

if len(infle) > 0 and flag==0: #to check whether the fuction has inflection point in the interval (-5,5)
#if flag is 1, which means the second derivative is always 0 in the domain(the graph of the function may be a straight line)
#in this case , there is no inflection points, eventhough the second derivative of the function is 0.
    print("x-value of inflection point",infle)
    for k in range(len(infle)):
        plt.plot(infle[k],p_sec(i))
        plt.annotate('%.2f' % infle[k],xy = (infle[k],p(infle[k])),ha='right',va='bottom',bbox=dict(boxstyle='round,pad=0.00000001',fc='green',alpha=0.5))
        #using its x-value to label the inflection points in green box.
else:
    print("No inflection point.") 
    #if there is no inflection point, we tell the user by print statement.

#draw the third graph
#the third graph includes the graph of the function and the inflection points if they exist
plt.plot()
plt.legend()
plt.show()


'''
Reflection

Melanie: The program is developed for finding derivative and graphing, our group members worked really hard. It is really challenge to choose a topic that we do not familiar with. 
    Before we started, we don’t even know how to write the codes. So we do lots of research about how to convert the methods of finding derivative to the computer codes. Some examples give us inspiration. 
    During the process, I found that although the program runs fluently, they were lack of several instructions for users. In my opinion, instruction is really significant in programming, it will affect user experience. So we write more comment in the program and make it more clearly. 
    Furthermore, in order to split the differentiation rules to computer code, I find some information on the Internet. I think it is a good method to learn by myself and develop our knowledge. However, because of the limit of time, we can only develop the graphing calculator for polynomial functions. 
    If we have enough time, I believe we can add more function and make the whole program completely.



Ryan: The purpose of our group was to use Python to develop a program which can  differentiate a function and draw the graph of a polynomial function. My task was to develop the graphing program which included graphing functions and finding critical points on the function. 
    In this part, I combined the knowledge that I acquired in calculus class and computer class and used something I hadn’t learned in class. At first, I sketched graphs of some simple functions successfully. However, I found the scale on x-axis and y-axis is too big so all the features of functions cannot be shown precisely besides end behavior. 
    Then I searched the information about graphing commands in Python online and found some useful commands “plot.ylim” and “plot.xlim” which can let computer only show the y-values and x-values in a particular interval. Additionally, I learned to use the command “plt.grid” which can make the graph neat. 
    After programming graphing function part, I turned to find the critical points on the function. I noticed it was hard to find the critical points precisely due to accuracy in computer. Our program always output x-values of the roots, local maximums, local minimums and inflection points with several x-values around the accurate one. 
    According to the binary search, I used a similar method to find the zeros in a function, the derivative of it and the second derivative of it accurately(I assume if f(x) and f(x+0.001) are different sign, the zeros is approximately x). Moreover, I used the command “round” to round the zeros to 2 decimals when I marked it on the graph. 
    Computer programming was very demanding in logical thinking and creativity. During the process of programming, I realized how to acquire knowledge from websites and use them in my code effectively. Furthermore, I learned how to solving the problems by applying the method similar to I learned before. 
    My dream is to study computer science in university, which requires students‘ ability in both mathematic and computer. I believed this experience was an essential step on my way to my dream.




Sandra: This project is a combination of calculus and computer programing, we develop the graphing program into the one that can find the roots, local values and inflection points, which demonstrates not only our understanding of calculus, but also the knowledge of coding. 
    The process of developing a method to find out the root/local values (roots of first derivative) /inflection points (roots of second derivative) of a function is hard but impressive.  
    At first, I tried to use the method that directly judges whether the y- value is 0 at a certain point, but as the root sometimes is not an integer or even is an irrational number, this method will cause some missing roots. 
    Then, I tried to test the y-value by comparing it to -0.0001 and 0.0001 to see whether it is in interval (-0.0001,0.0001), but the accuracy of this is hard to adjust. 
    If there is an integer root of function, the left side and right side of the root would be appended to the list of roots by mistake (ex. if root is 1, 0.99 and 1.01 can be in the list of roots through this method). 
    Also, if the root is irrational, the limitation in accuracy will cause missing roots. Then, I got the inspiration from left side and right side of the roots. Because if m is the root, f(m-0.001) and f(m+0.001) would have different signs, which means the product of them should be negative. 
    By applying this method, we can find the roots with higher accuracy. The formation of my idea shows the importance of self-reflection and basing one idea to one another to get new inspirations. Applying the knowledge of one subject to another helps me gain new knowledge during the process.


Eddy: Throughout the project, I have found and overcome new challenges. The main challenge I have been able to cope with consists of connecting two different scientific subjects, calculus and computer science. I am proud of the team work that happened during 
        the work project. By using Gitlab and Wechat groups, we have been able to organize and proceed with caution and cleanness. Furtunately, everyone in our group participates in the grade 11 computer science class which made our work very profound and effective.
        In the first few days, myself and others were confused with what we wanted to achieve in connecting calculus to computers as there is an infinite number of possibilities. For example, first we wanted to prove the defferentiation rules, however, we realized that 
        it was very short and easily achievable with only print statements. For this reason, we moved to differentiating functions and with research and practice we realized that the work we were overestimating can be solved easily thanks to a module called sympy and built-in functions such as 'diff'. 
        At last, we were able to agree on our focus that includes graphing polynomials with labels that evaluates the function in a certain range and domain. This project has allowed me to improve my critical thinking that is necessary to solve similar issues. 
        Computer programming is very complex and requires more research, otherwise, one might do in 50 lines what can be done in 5 lines which is why we were able to find few websites that taught us how to complete our tasks. 
        Nevertheless, our group work was not perfect, some flaws are more remarkable than others such as some features missing in the program that could be input with more time and research. 
        In order to improve, I would suggest working faster on different topics as we were usually working on the same topic at once and took a long time to decide what we should complete in this project. 
        As far as I am concerned, the program has achieved our expactation as it can complete the tasks we were aiming for and even better,one of the programs can solve multiple expressions at the same time.

'''
'''
                                                        Source Code:
                                                        -------------    
Mitchinson, Denise. “Python Advanced Course Topics.” Python Advanced: Polynomial Class, Bernd Klein, Bodenseo, 2011, www.python-course.eu/polynomial_class_in_python.php.
“9. Classes¶.” 9. Classes - Python 3.7.1 Documentation, Python Software Foundation, 25 Oct.	 2018, docs.python.org/3/tutorial/classes.html#generators.
“The Python Standard Library¶.” The Python Standard Library - Python 3.7.1 Documentation, Python Software Foundation, 25 Oct. 2018, docs.python.org/3/library/index.html.
“Calculus¶.” Calculus - SymPy 1.3 Documentation, SymPy Development Team, 14 Sept. 2018, docs.sympy.org/latest/modules/calculus/index.html.
'''

