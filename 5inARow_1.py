#5 In a Row Game, by Wyse Ebbah
#ICS4U February Assignment
import random
def displayGrid(m):
    for i in range(0,rowNo):
        for j in range(0,colNo):
            if m[i][j]==0:
                print('.', end="")
            elif m[i][j]==1:
                print('W', end="")
            else:
                print('B',end="")           
        print('')     



def chooseSquare():
    choiceMade=False
    while choiceMade==False:
        rowChoiceValid=False
        colChoiceValid=False
        while rowChoiceValid!=True:
            print('Enter the row of your selection')
            try:
                rowChoice=int(input())
                if rowChoice<rowNo:
                    rowChoiceValid=True
                else:
                    print('Input a number between 1 and '+str(rowNo))           
            except ValueError:
                print('Could you type in an integer')
        while colChoiceValid!=True:
            print('Enter the column of your selection')
            try:
                colChoice=int(input())
                if colChoice<rowNo:
                    colChoiceValid=True
                else:
                    print('Input a number between 1 and '+str(colNo))            
            except ValueError:
                print('Could you type in an integer')

        if grid[rowChoice-1][colChoice-1]==0:
            choiceMade=True
        else:
            print('That square has been taken, please try again')
            choiceMade=False
    return [rowChoice,colChoice]

def randomAI():
    avalSquares=[]
    for i in range(0,rowNo):
        for j in range(0,colNo):
            if grid[i][j]==0:
                avalSquares.append([i,j])
    avalSquaresNo=len(avalSquares)
    choiceNo=random.randint(1,avalSquaresNo)
    choiceSquare=avalSquares[choiceNo-1]
    return choiceSquare

#def checkSurroundingsAI():    
#    for i in range(0,rowNo):
#        for j in range(0,colNo):
#            if grid[rowNo][colNo]==1 or grid[rowNo][colNo]==2:
#                continue            
    
def checkWin():
    end=False
    for i in range(0,rowNo-4):
        for j in range(0,colNo-4):
            whiteNo=0
            blackNo=0
            for x in range (0,5):
                if grid[i+x][j]==1:
                    whiteNo=whiteNo+1
                elif grid[i+x][j]==2:
                    blackNo=blackNo+1                
            if whiteNo==5 or blackNo==5:
                end=True
                if whiteNo==5:
                    print('White has won')
                elif blackNo==5:
                    print('Black has won')
            whiteNo=0
            blackNo=0
            for x in range (0,5):
                if grid[i][j+x]==1:
                    whiteNo=whiteNo+1
                elif grid[i][j+x]==2:
                    blackNo=blackNo+1
            if whiteNo==5 or blackNo==5:
                end=True
                if whiteNo==5:
                    print('White has won')
                elif blackNo==5:
                    print('Black has won')
            whiteNo=0
            blackNo=0

            for x in range (0,5):
                if grid[i+x][j+x]==1:
                    whiteNo=whiteNo+1
                elif grid[i+x][j+x]==2:
                    blackNo=blackNo+1
            if whiteNo==5 or blackNo==5:
                end=True                    
                if whiteNo==5:
                    print('White has won')
                elif blackNo==5:
                    print('Black has won')           
            whiteNo=0
            blackNo=0

            for x in range (0,5):
                if grid[i+x][j-x]==1:
                    whiteNo=whiteNo+1
                elif grid[i+x][j-x]==2:
                    blackNo=blackNo+1
            if whiteNo==5 or blackNo==5:
                end=True                    
                if whiteNo==5:
                    print('White has won')
                elif blackNo==5:
                    print('Black has won')       
            whiteNo=0
            blackNo=0                          
    return end

def checkThreatA():
    stopSquare=[]   
    for i in range(0,rowNo-4):
        for j in range(0,rowNo-4):
            whiteNo=0
            blackNo=0
            emptyNo=0
            for x in range(0,5):
                if grid[i+x][j]==1:
                    whiteNo=whiteNo+1
                elif grid[i+x][j]==2:
                    blackNo=blackNo+1
                else:
                    emptyNo=emptyNo+1
            if whiteNo==4 and emptyNo==1:
                for x in range(0,5):
                    if grid[i+x][j]==0:
                        stopSquare.append([1,i+x,j])
            elif blackNo==4 and emptyNo==1:
                for x in range(0,5):
                    if grid[i+x][j]==0:
                        stopSquare.append([2,i+x,j])
            
            whiteNo=0
            blackNo=0
            emptyNo=0
            for x in range(0,5):
                if grid[i][j+x]==1:
                    whiteNo=whiteNo+1
                elif grid[i][j+x]==2:
                    blackNo=blackNo+1
                else:
                    emptyNo=emptyNo+1
            if whiteNo==4 and emptyNo==1:
                for x in range(0,5):
                    if grid[i][j+x]==0:
                        stopSquare.append([1,i,j+x])
            elif blackNo==4 and emptyNo==1:
                for x in range(0,5):
                    if grid[i][j+x]==0:
                        stopSquare.append([2,i,j+x])

            whiteNo=0
            blackNo=0
            emptyNo=0            
            for x in range(0,5):
                if grid[i+x][j+x]==1:
                    whiteNo=whiteNo+1
                elif grid[i+x][j+x]==2:
                    blackNo=blackNo+1
                else:
                    emptyNo=emptyNo+1
            if whiteNo==4 and emptyNo==1:
                for x in range(0,5):
                    if grid[i+x][j+x]==0:
                        stopSquare.append([1,i+x,j+x])
            elif blackNo==4 and emptyNo==1:
                for x in range(0,5):
                    if grid[i+x][j+x]==0:
                        stopSquare.append([2,i+x,j+x])

            for x in range(0,5):
                if grid[i+x][j-x]==1:
                    whiteNo=whiteNo+1
                elif grid[i+x][j-x]==2:
                    blackNo=blackNo+1
                else:
                    emptyNo=emptyNo+1
            if whiteNo==4 and emptyNo==1:
                for x in range(0,5):
                    if grid[i+x][j-x]==0:
                        stopSquare.append([1,i+x,j-x])
            elif blackNo==4 and emptyNo==1:
                for x in range(0,5):
                    if grid[i+x][j-x]==0:
                        stopSquare.append([2,i+x,j-x])
     
    return stopSquare

def playMultiplayer():
    gameOver=False
    while gameOver!=True: 
        displayGrid(grid)
        print('Black to move')
        squareVariables=chooseSquare()
        grid[squareVariables[0]-1][squareVariables[1]-1]=2
        gameOver=checkWin()
        if gameOver==True:
            break
        playerTurn='W'
        displayGrid(grid)
        print('White to move')
        squareVariables=chooseSquare()
        grid[squareVariables[0]-1][squareVariables[1]-1]=1
        gameOver=checkWin()
        playerTurn='B'

def playAI():
    gameOver=False
    print('Do you want to play black or white?')
    colorChoice=False
    while colorChoice == False:
        print("Please Input 'b' or 'w' to choose")
        colorChoice=input()  
        if colorChoice=='b' or colorChoice=='w':
            break
        else:
            colorChoice=False
    while gameOver!=True:
        if colorChoice=='b':
            displayGrid(grid)
            print('Black to Move')
            squareVariables=chooseSquare()
            grid[squareVariables[0]-1][squareVariables[1]-1]=2
            gameOver=checkWin()
            if gameOver==True:
                break
            playerTurn='W'
            displayGrid(grid)
            print('White to Move')
            try:
                AIVariables=checkThreatA()
                grid[AIVariables[0][1]][AIVariables[0][2]]=1
            except Exception:
                AIVariables=randomAI()
                grid[AIVariables[0]][AIVariables[1]]=1
            gameOver=checkWin()
            playerTurn='B'
        elif colorChoice=='w':
            displayGrid('grid')
            print('Black to Move')
            try:
                AIVariables=checkThreatA()
                grid[AIVariables[0][1]][AIVariables[0][2]]=2
            except Exception:
                AIVariables=randomAI()
                grid[AIVariables[0]][AIVariables[1]]=2
            gameOver=checkWin()
            if gameOver==True:
                break
            playerTurn='W'
            displayGrid(grid)
            print('White to Move')
            squareVariables=chooseSquare()
            grid[squareVariables[0]-1][squareVariables[1]-1]=1
            gameOver=checkWin()
            playerTurn='B'
        checkThreatA()

colNo=15
rowNo=15
grid = [[0 for x in range(0,colNo)] for y in range(0,rowNo)]
playerTurn='B'
print('Do you want to play Multiplayer or One Player')
gameChoice=False
while gameChoice==False:
    print("Please input 'Multiplayer' or 'One Player' exactly as described ")
    gameChoice=input()
    if gameChoice=='Multiplayer' or gameChoice=='One Player':
        break
    else:
        gameChoice=False
if gameChoice=='Multiplayer':
    playMultiplayer()
else:
    playAI()
displayGrid(grid)

