from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.button import ButtonBehavior
from kivy.uix.scatter import Scatter
from kivy. uix.floatlayout import FloatLayout
class tryApp(App):
    def build(self):
        f= FloatLayout()
        s= Scatter()
        L=Label(text=("Hi, there!"),font_size=(150))
        f.add_widget(s)
        s.add_widget(L)
        return f
tryApp().run()