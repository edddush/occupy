from graphics import * #pip3 install --user http://bit.ly/csc161graphics
import numpy as np
m=0
n=0
x1=0
y1=0
p=[[0 for a in range(15)] for b in range(15)]
white=[[0 for a in range(16)] for b in range(16)]
black=[[0 for a in range(16)] for b in range(16)]
q=[[0 for a in range(15)] for b in range(15)]

win=GraphWin('wuziqi',480,600)
win.setBackground("blue")
def WinBoard():
    for i in range(15):
       for j in range(15):
            p[i][j]=Point(i*30+30,j*30+30)
            p[i][j].draw(win)
    for r in range(15):
        Line(p[r][0],p[r][14]).draw(win)
    for s in range(15):
        Line(p[0][s],p[14][s]).draw(win)
    center=Circle(p[7][7],3)
    center.draw(win)
    center.setFill('black')

def Click(x1,y1):
    p1=win.getMouse()
    x1=p1.getX()
    y1=p1.getY()
    for i in range(15):
        for j in range(15):
            sqrdis=((x1-p[i][j].getX())*(x1-p[i][j].getX()))+((y1-p[i][j].getY())*(y1-p[i][j].getY()))
            if sqrdis<=200 and q[i][j]==0:
                white[i+1][j+1]=1
                q[i][j]=Circle(p[i][j],10)
                q[i][j].draw(win)      
                q[i][j].setFill('white')
                x1=i
                y1=j
    return [m,x1,y1]

def Check():
    for i in range(15):
        for j in range(11):
            if white[i+1][j+1] and white[i+1][j+2] and white[i+1][j+3] and white[i+1][j+4] and white[i+1][j+5]:
               return 1
               break
            if black[i+1][j+1] and black[i+1][j+2]and black[i+1][j+3]and black[i+1][j+4]and black[i+1][j+5]:
               return 2
               break
    for i in range(11):
        for j in range(15):
            if white[i+1][j+1] and white[i+2][j+1]and white[i+3][j+1]and white[i+4][j+1]and white[i+5][j+1]:
               return 1
               break
            if black[i+1][j+1] and black[i+2][j+1]and black[i+3][j+1]and black[i+4][j+1]and black[i+5][j+1]:
               return 2
               break
    for i in range(11):
        for j in range(11):
            if white[i+1][j+1] and white[i+2][j+2]and white[i+3][j+3]and white[i+4][j+4]and white[i+5][j+5]:
               return 1
               break
            if black[i+1][j+1] and black[i+2][j+2]and black[i+3][j+3]and black[i+4][j+4]and black[i+5][j+5]:
               return 2
               break
    for i in range(11):
        for j in range(15):
            if white[i+1][j+1] and white[i+2][j]and white[i+3][j-1]and white[i+4][j-2]and white[i+5][j-3]:
               return 1
               break
            if black[i+1][j+1] and black[i+2][j]and black[i+3][j-1]and black[i+4][j-2]and black[i+5][j-3]:
               return 2
               break

def main():
    WinBoard()
    m=0
    n=0
    x1=0
    y1=0
    black[8][8]=1
    q[7][7]=Circle(p[7][7],10)
    q[7][7].draw(win)      
    q[7][7].setFill('black')
    while 1:
        Check()
        if Check()==1:
            Text(Point(240,550),'the white wins').draw(win)
            break
        if Check()==2:
            Text(Point(240,550),'the black wins').draw(win)
            break
        [m,x1,y1]=Click(x1,y1)
        Check()
        if Check()==1:
            Text(Point(240,550),'the white wins').draw(win)
            break
        if Check()==2:
            Text(Point(240,550),'the black wins').draw(win)
            break
        
        for i in range(15):
            for j in range(11):
                if black[i+1][j+1] and black[i+1][j+2] and black[i+1][j+3] and black[i+1][j+4] and q[i][j+4]==0:
                    black[i+1][j+5]=1
                    q[i][j+4]=Circle(p[i][j+4],10)
                    q[i][j+4].draw(win)      
                    q[i][j+4].setFill('black')
                    n=1
                    break
                elif black[i+1][j+1] and black[i+1][j+2] and black[i+1][j+3] and black[i+1][j+4] and q[i][j-1]==0:
                    black[i+1][j]=1
                    q[i][j-1]=Circle(p[i][j-1],10)
                    q[i][j-1].draw(win)      
                    q[i][j-1].setFill('black')
                    n=1
                    break
                elif black[i+1][j+1] and black[i+1][j+2] and black[i+1][j+3] and black[i+1][j+5] and q[i][j+3]==0:
                    black[i+1][j+4]=1
                    q[i][j+3]=Circle(p[i][j+3],10)
                    q[i][j+3].draw(win)      
                    q[i][j+3].setFill('black')
                    n=1
                    break
                elif black[i+1][j+1] and black[i+1][j+2] and black[i+1][j+4] and black[i+1][j+5] and q[i][j+2]==0:
                    black[i+1][j+3]=1
                    q[i][j+2]=Circle(p[i][j+2],10)
                    q[i][j+2].draw(win)      
                    q[i][j+2].setFill('black')
                    n=1
                    break
                elif black[i+1][j+1] and black[i+1][j+3] and black[i+1][j+4] and black[i+1][j+5] and q[i][j+1]==0:
                    black[i+1][j+2]=1
                    q[i][j+1]=Circle(p[i][j+1],10)
                    q[i][j+1].draw(win)      
                    q[i][j+1].setFill('black')
                    n=1
                    break
            if n==1:    
                break
        if n==1:
            n=0
            continue              

        for i in range(11):
            for j in range(15):    
                if black[i+1][j+1] and black[i+2][j+1]and black[i+3][j+1] and black[i+4][j+1] and q[i+4][j]==0:
                    black[i+5][j+1]=1
                    q[i+4][j]=Circle(p[i+4][j],10)
                    q[i+4][j].draw(win)      
                    q[i+4][j].setFill('black')
                    n=1
                    break
                elif black[i+1][j+1] and black[i+2][j+1] and black[i+3][j+1] and black[i+4][j+1] and q[i-1][j]==0:
                    black[i][j+1]=1
                    q[i-1][j]=Circle(p[i-1][j],10)
                    q[i-1][j].draw(win)      
                    q[i-1][j].setFill('black')
                    n=1
                    break
                elif black[i+1][j+1] and black[i+2][j+1] and black[i+3][j+1] and black[i+5][j+1] and q[i+3][j]==0:
                    black[i+4][j+1]=1
                    q[i+3][j]=Circle(p[i+3][j],10)
                    q[i+3][j].draw(win)      
                    q[i+3][j].setFill('black')
                    n=1
                    break
                elif black[i+1][j+1] and black[i+2][j+1] and black[i+4][j+1] and black[i+5][j+1] and q[i+2][j]==0:
                    black[i+3][j+1]=1
                    q[i+2][j]=Circle(p[i+2][j],10)
                    q[i+2][j].draw(win)      
                    q[i+2][j].setFill('black')
                    n=1
                    break
                elif black[i+1][j+1] and black[i+3][j+1] and black[i+4][j+1] and black[i+5][j+1] and q[i+1][j]==0:
                    black[i+2][j+1]=1
                    q[i+1][j]=Circle(p[i+1][j],10)
                    q[i+1][j].draw(win)      
                    q[i+1][j].setFill('black')
                    n=1
                    break
            if n==1:   
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(11):    
                if black[i+1][j+1] and black[i+2][j+2]and black[i+3][j+3] and black[i+4][j+4] and q[i+4][j+4]==0:
                    black[i+5][j+5]=1
                    q[i+4][j+4]=Circle(p[i+4][j+4],10)
                    q[i+4][j+4].draw(win)      
                    q[i+4][j+4].setFill('black')
                    n=1
                    break
                elif black[i+1][j+1] and black[i+2][j+2]and black[i+3][j+3] and black[i+4][j+4] and q[i-1][j-1]==0:
                    black[i][j]=1
                    q[i-1][j-1]=Circle(p[i-1][j-1],10)
                    q[i-1][j-1].draw(win)      
                    q[i-1][j-1].setFill('black')
                    n=1
                    break
                elif black[i+1][j+1] and black[i+2][j+2]and black[i+3][j+3] and black[i+5][j+5] and q[i+3][j+3]==0:
                    black[i+4][j+4]=1
                    q[i+3][j+3]=Circle(p[i+3][j+3],10)
                    q[i+3][j+3].draw(win)      
                    q[i+3][j+3].setFill('black')
                    n=1
                    break
                elif black[i+1][j+1] and black[i+2][j+2]and black[i+4][j+4] and black[i+5][j+5] and q[i+2][j+2]==0:
                    black[i+3][j+3]=1
                    q[i+2][j+2]=Circle(p[i+2][j+2],10)
                    q[i+2][j+2].draw(win)      
                    q[i+2][j+2].setFill('black')
                    n=1
                    break
                elif black[i+1][j+1] and black[i+3][j+3]and black[i+4][j+4] and black[i+5][j+5] and q[i+1][j+1]==0:
                    black[i+2][j+2]=1
                    q[i+1][j+1]=Circle(p[i+1][j+1],10)
                    q[i+1][j+1].draw(win)      
                    q[i+1][j+1].setFill('black')
                    n=1
                    break
            if n==1:    
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(15):    
                if black[i+1][j+1] and black[i+2][j]and black[i+3][j-1]and black[i+4][j-2] and q[i+4][j-4]==0:
                    black[i+5][j-3]=1
                    q[i+4][j-4]=Circle(p[i+4][j-4],10)
                    q[i+4][j-4].draw(win)      
                    q[i+4][j-4].setFill('black')
                    n=1
                    break
                elif black[i+1][j+1] and black[i+2][j]and black[i+3][j-1]and black[i+4][j-2] and q[i-1][j+1]==0:
                    black[i][j+2]=1
                    q[i-1][j+1]=Circle(p[i-1][j+1],10)
                    q[i-1][j+1].draw(win)      
                    q[i-1][j+1].setFill('black')
                    n=1
                    break
                elif black[i+1][j+1] and black[i+2][j]and black[i+3][j-1]and black[i+5][j-3] and q[i+3][j-3]==0:
                    black[i+4][j-2]=1
                    q[i+3][j-3]=Circle(p[i+3][j-3],10)
                    q[i+3][j-3].draw(win)      
                    q[i+3][j-3].setFill('black')
                    n=1
                    break
                elif black[i+1][j+1] and black[i+2][j]and black[i+4][j-2]and black[i+5][j-3] and q[i+2][j-2]==0:
                    black[i+3][j-1]=1
                    q[i+2][j-2]=Circle(p[i+2][j-2],10)
                    q[i+2][j-2].draw(win)      
                    q[i+2][j-2].setFill('black')
                    n=1
                    break
                elif black[i+1][j+1] and black[i+3][j-1]and black[i+4][j-2]and black[i+5][j-3] and q[i+1][j-1]==0:
                    black[i+2][j]=1
                    q[i+1][j-1]=Circle(p[i+1][j-1],10)
                    q[i+1][j-1].draw(win)      
                    q[i+1][j-1].setFill('black')
                    n=1
                    break
            if n==1:    
                break
        if n==1:
            n=0
            continue
        
        for i in range(15):
            for j in range(11):
                if white[i+1][j+1] and white[i+1][j+2] and white[i+1][j+3] and white[i+1][j+4] and q[i][j+4]==0:
                    black[i+1][j+5]=1
                    q[i][j+4]=Circle(p[i][j+4],10)
                    q[i][j+4].draw(win)      
                    q[i][j+4].setFill('black')
                    n=1
                elif white[i+1][j+1] and white[i+1][j+2] and white[i+1][j+3] and white[i+1][j+4] and q[i][j-1]==0:
                    black[i+1][j]=1
                    q[i][j-1]=Circle(p[i][j-1],10)
                    q[i][j-1].draw(win)      
                    q[i][j-1].setFill('black')
                    n=1
                elif white[i+1][j+1] and white[i+1][j+2] and white[i+1][j+3] and white[i+1][j+5] and q[i][j+3]==0:
                    black[i+1][j+4]=1
                    q[i][j+3]=Circle(p[i][j+3],10)
                    q[i][j+3].draw(win)      
                    q[i][j+3].setFill('black')
                    n=1
                elif white[i+1][j+1] and white[i+1][j+2] and white[i+1][j+4] and white[i+1][j+5] and q[i][j+2]==0:
                    black[i+1][j+3]=1
                    q[i][j+2]=Circle(p[i][j+2],10)
                    q[i][j+2].draw(win)      
                    q[i][j+2].setFill('black')
                    n=1
                elif white[i+1][j+1] and white[i+1][j+3] and white[i+1][j+4] and white[i+1][j+5] and q[i][j+1]==0:
                    black[i+1][j+2]=1
                    q[i][j+1]=Circle(p[i][j+1],10)
                    q[i][j+1].draw(win)      
                    q[i][j+1].setFill('black')
                    n=1       
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(15):           
                if white[i+1][j+1] and white[i+2][j+1]and white[i+3][j+1] and white[i+4][j+1] and q[i+4][j]==0:
                    black[i+5][j+1]=1
                    q[i+4][j]=Circle(p[i+4][j],10)
                    q[i+4][j].draw(win)      
                    q[i+4][j].setFill('black')
                    n=1
                elif white[i+1][j+1] and white[i+2][j+1] and white[i+3][j+1] and white[i+4][j+1] and q[i-1][j]==0:
                    black[i][j+1]=1
                    q[i-1][j]=Circle(p[i-1][j],10)
                    q[i-1][j].draw(win)      
                    q[i-1][j].setFill('black')
                    n=1
                elif white[i+1][j+1] and white[i+2][j+1] and white[i+3][j+1] and white[i+5][j+1] and q[i+3][j]==0:
                    black[i+4][j+1]=1
                    q[i+3][j]=Circle(p[i+3][j],10)
                    q[i+3][j].draw(win)      
                    q[i+3][j].setFill('black')
                    n=1
                elif white[i+1][j+1] and white[i+2][j+1] and white[i+4][j+1] and white[i+5][j+1] and q[i+2][j]==0:
                    black[i+3][j+1]=1
                    q[i+2][j]=Circle(p[i+2][j],10)
                    q[i+2][j].draw(win)      
                    q[i+2][j].setFill('black')
                    n=1
                elif white[i+1][j+1] and white[i+3][j+1] and white[i+4][j+1] and white[i+5][j+1] and q[i+1][j]==0:
                    black[i+2][j+1]=1
                    q[i+1][j]=Circle(p[i+1][j],10)
                    q[i+1][j].draw(win)      
                    q[i+1][j].setFill('black')
                    n=1
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(11):
                if white[i+1][j+1] and white[i+2][j+2]and white[i+3][j+3] and white[i+4][j+4] and q[i+4][j+4]==0:
                    black[i+5][j+5]=1
                    q[i+4][j+4]=Circle(p[i+4][j+4],10)
                    q[i+4][j+4].draw(win)      
                    q[i+4][j+4].setFill('black')
                    n=1
                elif white[i+1][j+1] and white[i+2][j+2]and white[i+3][j+3] and white[i+4][j+4] and q[i-1][j-1]==0:
                    black[i][j]=1
                    q[i-1][j-1]=Circle(p[i-1][j-1],10)
                    q[i-1][j-1].draw(win)      
                    q[i-1][j-1].setFill('black')
                    n=1
                elif white[i+1][j+1] and white[i+2][j+2]and white[i+3][j+3] and white[i+5][j+5] and q[i+3][j+3]==0:
                    black[i+4][j+4]=1
                    q[i+3][j+3]=Circle(p[i+3][j+3],10)
                    q[i+3][j+3].draw(win)      
                    q[i+3][j+3].setFill('black')
                    n=1
                elif white[i+1][j+1] and white[i+2][j+2]and white[i+4][j+4] and white[i+5][j+5] and q[i+2][j+2]==0:
                    black[i+3][j+3]=1
                    q[i+2][j+2]=Circle(p[i+2][j+2],10)
                    q[i+2][j+2].draw(win)      
                    q[i+2][j+2].setFill('black')
                    n=1
                elif white[i+1][j+1] and white[i+3][j+3]and white[i+4][j+4] and white[i+5][j+5] and q[i+1][j+1]==0:
                    black[i+2][j+2]=1
                    q[i+1][j+1]=Circle(p[i+1][j+1],10)
                    q[i+1][j+1].draw(win)      
                    q[i+1][j+1].setFill('black')
                    n=1
        if n==1:
            n=0
            continue   

        for i in range(11):
            for j in range(15):
                if white[i+1][j+1] and white[i+2][j]and white[i+3][j-1]and white[i+4][j-2] and q[i+4][j-4]==0:
                    black[i+5][j-3]=1
                    q[i+4][j-4]=Circle(p[i+4][j-4],10)
                    q[i+4][j-4].draw(win)      
                    q[i+4][j-4].setFill('black')
                    n=1
                elif white[i+1][j+1] and white[i+2][j]and white[i+3][j-1]and white[i+4][j-2] and q[i-1][j+1]==0:
                    black[i][j+2]=1
                    q[i-1][j+1]=Circle(p[i-1][j+1],10)
                    q[i-1][j+1].draw(win)      
                    q[i-1][j+1].setFill('black')
                    n=1
                elif white[i+1][j+1] and white[i+2][j]and white[i+3][j-1]and white[i+5][j-3] and q[i+3][j-3]==0:
                    black[i+4][j-2]=1
                    q[i+3][j-3]=Circle(p[i+3][j-3],10)
                    q[i+3][j-3].draw(win)      
                    q[i+3][j-3].setFill('black')
                    n=1
                elif white[i+1][j+1] and white[i+2][j]and white[i+4][j-2]and white[i+5][j-3] and q[i+2][j-2]==0:
                    black[i+3][j-1]=1
                    q[i+2][j-2]=Circle(p[i+2][j-2],10)
                    q[i+2][j-2].draw(win)      
                    q[i+2][j-2].setFill('black')
                    n=1
                elif white[i+1][j+1] and white[i+3][j-1]and white[i+4][j-2]and white[i+5][j-3] and q[i+1][j-1]==0:
                    black[i+2][j]=1
                    q[i+1][j-1]=Circle(p[i+1][j-1],10)
                    q[i+1][j-1].draw(win)      
                    q[i+1][j-1].setFill('black')
                    n=1
        if n==1:
            n=0
            continue
        
        for i in range(15):
            for j in range(11):    
                if black[i+1][j+1] and black[i+1][j+2] and black[i+1][j+3] and q[i][j-1]==0 and q[i][j+3]==0:
                    black[i+1][j+4]=1
                    q[i][j+3]=Circle(p[i][j+3],10)
                    q[i][j+3].draw(win)      
                    q[i][j+3].setFill('black')
                    n=1
                    break
                elif black[i+1][j+1] and black[i+1][j+2] and black[i+1][j+3] and q[i][j-1]==0 and q[i][j+3]==0:
                    black[i+1][j]=1
                    q[i][j-1]=Circle(p[i][j-1],10)
                    q[i][j-1].draw(win)      
                    q[i][j-1].setFill('black')
                    n=1
                    break
                elif black[i+1][j+1] and black[i+1][j+2] and black[i+1][j+4] and q[i][j-1]==0 and q[i][j+4]==0 and q[i][j+2]==0:
                    black[i+1][j+3]=1
                    q[i][j+2]=Circle(p[i][j+2],10)
                    q[i][j+2].draw(win)      
                    q[i][j+2].setFill('black')
                    n=1
                    break
                elif black[i+1][j+1] and black[i+1][j+3] and black[i+1][j+4] and q[i][j-1]==0 and q[i][j+4]==0 and q[i][j+1]==0:
                    black[i+1][j+2]=1
                    q[i][j+1]=Circle(p[i][j+1],10)
                    q[i][j+1].draw(win)      
                    q[i][j+1].setFill('black')
                    n=1
                    break
            if n==1:    
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(15):              
                if black[i+1][j+1] and black[i+2][j+1]and black[i+3][j+1] and q[i-1][j]==0 and q[i+3][j]==0:
                    black[i+4][j+1]=1
                    q[i+3][j]=Circle(p[i+3][j],10)
                    q[i+3][j].draw(win)      
                    q[i+3][j].setFill('black')
                    n=1
                    break
                elif black[i+1][j+1] and black[i+2][j+1] and black[i+3][j+1] and q[i-1][j]==0 and q[i+3][j]==0:
                    black[i][j+1]=1
                    q[i-1][j]=Circle(p[i-1][j],10)
                    q[i-1][j].draw(win)      
                    q[i-1][j].setFill('black')
                    n=1
                    break
                elif black[i+1][j+1] and black[i+2][j+1] and black[i+4][j+1] and q[i-1][j]==0 and q[i+4][j]==0 and q[i+2][j]==0:
                    black[i+3][j+1]=1
                    q[i+2][j]=Circle(p[i+2][j],10)
                    q[i+2][j].draw(win)      
                    q[i+2][j].setFill('black')
                    n=1
                    break
                elif black[i+1][j+1] and black[i+3][j+1] and black[i+4][j+1] and q[i-1][j]==0 and q[i+4][j]==0 and q[i+1][j]==0:
                    black[i+2][j+1]=1
                    q[i+1][j]=Circle(p[i+1][j],10)
                    q[i+1][j].draw(win)      
                    q[i+1][j].setFill('black')
                    n=1
                    break
            if n==1:    
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(11):    
                if black[i+1][j+1] and black[i+2][j+2]and black[i+3][j+3] and q[i-1][j-1]==0 and q[i+3][j+3]==0:
                    black[i+4][j+4]=1
                    q[i+3][j+3]=Circle(p[i+3][j+3],10)
                    q[i+3][j+3].draw(win)      
                    q[i+3][j+3].setFill('black')
                    n=1
                    break
                elif black[i+1][j+1] and black[i+2][j+2]and black[i+3][j+3] and q[i-1][j-1]==0 and q[i+3][j+3]==0:
                    black[i][j]=1
                    q[i-1][j-1]=Circle(p[i-1][j-1],10)
                    q[i-1][j-1].draw(win)      
                    q[i-1][j-1].setFill('black')
                    n=1
                    break              
                elif black[i+1][j+1] and black[i+2][j+2]and black[i+4][j+4] and q[i-1][j-1]==0 and q[i+4][j+4]==0 and q[i+2][j+2]==0:
                    black[i+3][j+3]=1
                    q[i+2][j+2]=Circle(p[i+2][j+2],10)
                    q[i+2][j+2].draw(win)      
                    q[i+2][j+2].setFill('black')
                    n=1
                    break                 
                elif black[i+1][j+1] and black[i+3][j+3]and black[i+4][j+4] and q[i-1][j-1]==0 and q[i+4][j+4]==0 and q[i+1][j+1]==0:
                    black[i+2][j+2]=1
                    q[i+1][j+1]=Circle(p[i+1][j+1],10)
                    q[i+1][j+1].draw(win)      
                    q[i+1][j+1].setFill('black')
                    n=1
                    break
            if n==1:    
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(15):    
                if black[i+1][j+1] and black[i+2][j]and black[i+3][j-1]and q[i-1][j+1]==0 and q[i+3][j-3]==0:
                    black[i+4][j-2]=1
                    q[i+3][j-3]=Circle(p[i+3][j-3],10)
                    q[i+3][j-3].draw(win)      
                    q[i+3][j-3].setFill('black')
                    n=1
                    break
                elif black[i+1][j+1] and black[i+2][j]and black[i+3][j-1]and q[i-1][j+1]==0 and q[i+3][j-3]==0:
                    black[i][j+2]=1
                    q[i-1][j+1]=Circle(p[i-1][j+1],10)
                    q[i-1][j+1].draw(win)      
                    q[i-1][j+1].setFill('black')
                    n=1
                    break
                elif black[i+1][j+1] and black[i+2][j]and black[i+4][j-2]and q[i-1][j+1]==0 and q[i+4][j-4]==0 and q[i+2][j-2]==0:
                    black[i+3][j-1]=1
                    q[i+2][j-2]=Circle(p[i+2][j-2],10)
                    q[i+2][j-2].draw(win)      
                    q[i+2][j-2].setFill('black')
                    n=1
                    break
                elif black[i+1][j+1] and black[i+3][j-1]and black[i+4][j-2]and q[i-1][j+1]==0 and q[i+4][j-4]==0 and q[i+1][j-1]==0:
                    black[i+2][j]=1
                    q[i+1][j-1]=Circle(p[i+1][j-1],10)
                    q[i+1][j-1].draw(win)      
                    q[i+1][j-1].setFill('black')
                    n=1
                    break
            if n==1:    
                break
        if n==1:
            n=0
            continue   
                
        for i in range(15):
            for j in range(11):
                if white[i+1][j+1] and white[i+1][j+2] and white[i+1][j+3] and q[i][j-1]==0 and q[i][j+3]==0:
                    black[i+1][j+4]=1
                    q[i][j+3]=Circle(p[i][j+3],10)
                    q[i][j+3].draw(win)      
                    q[i][j+3].setFill('black')
                    n=1
                elif white[i+1][j+1] and white[i+1][j+2] and white[i+1][j+3] and q[i][j-1]==0 and q[i][j+3]==0:
                    black[i+1][j]=1
                    q[i][j-1]=Circle(p[i][j-1],10)
                    q[i][j-1].draw(win)      
                    q[i][j-1].setFill('black')
                    n=1
                elif white[i+1][j+1] and white[i+1][j+2] and white[i+1][j+4] and q[i][j-1]==0 and q[i][j+4]==0 and q[i][j+2]==0:
                    black[i+1][j+3]=1
                    q[i][j+2]=Circle(p[i][j+2],10)
                    q[i][j+2].draw(win)      
                    q[i][j+2].setFill('black')
                    n=1
                elif white[i+1][j+1] and white[i+1][j+3] and white[i+1][j+4] and q[i][j-1]==0 and q[i][j+4]==0 and q[i][j+1]==0:
                    black[i+1][j+2]=1
                    q[i][j+1]=Circle(p[i][j+1],10)
                    q[i][j+1].draw(win)      
                    q[i][j+1].setFill('black')
                    n=1
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(15):           
                if white[i+1][j+1] and white[i+2][j+1]and white[i+3][j+1] and q[i-1][j]==0 and q[i+3][j]==0:
                    black[i+4][j+1]=1
                    q[i+3][j]=Circle(p[i+3][j],10)
                    q[i+3][j].draw(win)      
                    q[i+3][j].setFill('black')
                    n=1
                elif white[i+1][j+1] and white[i+2][j+1] and white[i+3][j+1] and q[i-1][j]==0 and q[i+3][j]==0:
                    black[i][j+1]=1
                    q[i-1][j]=Circle(p[i-1][j],10)
                    q[i-1][j].draw(win)      
                    q[i-1][j].setFill('black')
                    n=1
                elif white[i+1][j+1] and white[i+2][j+1] and white[i+4][j+1] and q[i-1][j]==0 and q[i+4][j]==0 and q[i+2][j]==0:
                    black[i+3][j+1]=1
                    q[i+2][j]=Circle(p[i+2][j],10)
                    q[i+2][j].draw(win)      
                    q[i+2][j].setFill('black')
                    n=1
                elif white[i+1][j+1] and white[i+3][j+1] and white[i+4][j+1] and q[i-1][j]==0 and q[i+4][j]==0 and q[i+1][j]==0:
                    black[i+2][j+1]=1
                    q[i+1][j]=Circle(p[i+1][j],10)
                    q[i+1][j].draw(win)      
                    q[i+1][j].setFill('black')
                    n=1
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(11):
                if white[i+1][j+1] and white[i+2][j+2]and white[i+3][j+3] and q[i-1][j-1]==0 and q[i+3][j+3]==0:
                    black[i+4][j+4]=1
                    q[i+3][j+3]=Circle(p[i+3][j+3],10)
                    q[i+3][j+3].draw(win)      
                    q[i+3][j+3].setFill('black')
                    n=1
                elif white[i+1][j+1] and white[i+2][j+2]and white[i+3][j+3] and q[i-1][j-1]==0 and q[i+3][j+3]==0:
                    black[i][j]=1
                    q[i-1][j-1]=Circle(p[i-1][j-1],10)
                    q[i-1][j-1].draw(win)      
                    q[i-1][j-1].setFill('black')
                    n=1
                elif white[i+1][j+1] and white[i+2][j+2]and white[i+4][j+4] and q[i-1][j-1]==0 and q[i+4][j+4]==0 and q[i+2][j+2]==0:
                    black[i+3][j+3]=1
                    q[i+2][j+2]=Circle(p[i+2][j+2],10)
                    q[i+2][j+2].draw(win)      
                    q[i+2][j+2].setFill('black')
                    n=1
                elif white[i+1][j+1] and white[i+3][j+3]and white[i+4][j+4] and q[i-1][j-1]==0 and q[i+4][j+4]==0 and q[i+1][j+1]==0:
                    black[i+2][j+2]=1
                    q[i+1][j+1]=Circle(p[i+1][j+1],10)
                    q[i+1][j+1].draw(win)      
                    q[i+1][j+1].setFill('black')
                    n=1
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(15):
                if white[i+1][j+1] and white[i+2][j]and white[i+3][j-1]and q[i-1][j+1]==0 and q[i+3][j-3]==0:
                    black[i+4][j-2]=1
                    q[i+3][j-3]=Circle(p[i+3][j-3],10)
                    q[i+3][j-3].draw(win)      
                    q[i+3][j-3].setFill('black')
                    n=1
                elif white[i+1][j+1] and white[i+2][j]and white[i+3][j-1]and q[i-1][j+1]==0 and q[i+3][j-3]==0:
                    black[i][j+2]=1
                    q[i-1][j+1]=Circle(p[i-1][j+1],10)
                    q[i-1][j+1].draw(win)      
                    q[i-1][j+1].setFill('black')
                    n=1
                elif white[i+1][j+1] and white[i+2][j]and white[i+4][j-2]and q[i-1][j+1]==0 and q[i+4][j-4]==0 and q[i+2][j-2]==0:
                    black[i+3][j-1]=1
                    q[i+2][j-2]=Circle(p[i+2][j-2],10)
                    q[i+2][j-2].draw(win)      
                    q[i+2][j-2].setFill('black')
                    n=1
                elif white[i+1][j+1] and white[i+3][j-1]and white[i+4][j-2]and q[i-1][j+1]==0 and q[i+4][j-4]==0 and q[i+1][j-1]==0:
                    black[i+2][j]=1
                    q[i+1][j-1]=Circle(p[i+1][j-1],10)
                    q[i+1][j-1].draw(win)      
                    q[i+1][j-1].setFill('black')
                    n=1
        if n==1:
            n=0
            continue

        

        for i in range(12):
            for j in range(11):         
                if black[i][j+1] and black[i+2][j+1] and black[i+1][j+2] and black[i+1][j+3] and q[i-2][j]==0 and q[i+2][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-3][j]!=0 and q[i+3][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i+1][j+1]=1
                        q[i][j]=Circle(p[i][j],10)
                        q[i][j].draw(win)      
                        q[i][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(12):
            for j in range(13):         
                if black[i][j+1] and black[i+2][j+1] and black[i+1][j] and black[i+1][j-1] and q[i-2][j]==0 and q[i+2][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i-3][j]!=0 and q[i+3][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i+1][j+1]=1
                        q[i][j]=Circle(p[i][j],10)
                        q[i][j].draw(win)      
                        q[i][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(12):         
                if black[i+1][j] and black[i+1][j+2] and black[i+2][j+1] and black[i+3][j+1] and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i+1][j+1]=1
                        q[i][j]=Circle(p[i][j],10)
                        q[i][j].draw(win)      
                        q[i][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(13):
            for j in range(12):         
                if black[i+1][j] and black[i+1][j+2] and black[i][j+1] and black[i-1][j+1] and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i+2][j]!=0 and q[i-4][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i+1][j+1]=1
                        q[i][j]=Circle(p[i][j],10)
                        q[i][j].draw(win)      
                        q[i][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(12):
            for j in range(12):         
                if black[i+1][j] and black[i+1][j+2] and black[i][j+1] and black[i+2][j+1] and q[i+2][j]==0 and q[i-2][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i+3][j]!=0 and q[i-3][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i+1][j+1]=1
                        q[i][j]=Circle(p[i][j],10)
                        q[i][j].draw(win)      
                        q[i][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue                
        
        for i in range(11):
            for j in range(11):         
                if black[i+3][j+1] and black[i+2][j+1] and black[i+1][j+2] and black[i+1][j+3] and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i+1][j+1]=1
                        q[i][j]=Circle(p[i][j],10)
                        q[i][j].draw(win)      
                        q[i][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(13):         
                if black[i+3][j+1] and black[i+2][j+1] and black[i+1][j] and black[i+1][j-1] and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i+1][j+1]=1
                        q[i][j]=Circle(p[i][j],10)
                        q[i][j].draw(win)      
                        q[i][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(13):
            for j in range(11):         
                if black[i+1][j+3] and black[i+1][j+2] and black[i][j+1] and black[i-1][j+1] and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-4][j]!=0 and q[i+2][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i+1][j+1]=1
                        q[i][j]=Circle(p[i][j],10)
                        q[i][j].draw(win)      
                        q[i][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(13):
            for j in range(13):         
                if black[i+1][j] and black[i+1][j-1] and black[i][j+1] and black[i-1][j+1] and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i+2][j]!=0 and q[i-4][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i+1][j+1]=1
                        q[i][j]=Circle(p[i][j],10)
                        q[i][j].draw(win)      
                        q[i][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(12):
            for j in range(11):         
                if white[i][j+1] and white[i+2][j+1] and white[i+1][j+2] and white[i+1][j+3] and q[i-2][j]==0 and q[i+2][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-3][j]!=0 and q[i+3][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i+1][j+1]=1
                        q[i][j]=Circle(p[i][j],10)
                        q[i][j].draw(win)      
                        q[i][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(12):
            for j in range(13):         
                if white[i][j+1] and white[i+2][j+1] and white[i+1][j] and white[i+1][j-1] and q[i-2][j]==0 and q[i+2][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i-3][j]!=0 and q[i+3][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i+1][j+1]=1
                        q[i][j]=Circle(p[i][j],10)
                        q[i][j].draw(win)      
                        q[i][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(12):         
                if white[i+1][j] and white[i+1][j+2] and white[i+2][j+1] and white[i+3][j+1] and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i+1][j+1]=1
                        q[i][j]=Circle(p[i][j],10)
                        q[i][j].draw(win)      
                        q[i][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(13):
            for j in range(12):         
                if white[i+1][j] and white[i+1][j+2] and white[i][j+1] and white[i-1][j+1] and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i+2][j]!=0 and q[i-4][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i+1][j+1]=1
                        q[i][j]=Circle(p[i][j],10)
                        q[i][j].draw(win)      
                        q[i][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(12):
            for j in range(12):         
                if white[i+1][j] and white[i+1][j+2] and white[i][j+1] and white[i+2][j+1] and q[i+2][j]==0 and q[i-2][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i+3][j]!=0 and q[i-3][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i+1][j+1]=1
                        q[i][j]=Circle(p[i][j],10)
                        q[i][j].draw(win)      
                        q[i][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue                
        
        for i in range(11):
            for j in range(11):         
                if white[i+3][j+1] and white[i+2][j+1] and white[i+1][j+2] and white[i+1][j+3] and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i+1][j+1]=1
                        q[i][j]=Circle(p[i][j],10)
                        q[i][j].draw(win)      
                        q[i][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(13):         
                if white[i+3][j+1] and white[i+2][j+1] and white[i+1][j] and white[i+1][j-1] and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i+1][j+1]=1
                        q[i][j]=Circle(p[i][j],10)
                        q[i][j].draw(win)      
                        q[i][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(13):
            for j in range(11):         
                if white[i+1][j+3] and white[i+1][j+2] and white[i][j+1] and white[i-1][j+1] and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-4][j]!=0 and q[i+2][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i+1][j+1]=1
                        q[i][j]=Circle(p[i][j],10)
                        q[i][j].draw(win)      
                        q[i][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(13):
            for j in range(13):         
                if white[i+1][j] and white[i+1][j-1] and white[i][j+1] and white[i-1][j+1] and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i+2][j]!=0 and q[i-4][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i+1][j+1]=1
                        q[i][j]=Circle(p[i][j],10)
                        q[i][j].draw(win)      
                        q[i][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(12):
            for j in range(12):         
                if white[i+1][j] and white[i+1][j+2] and white[i][j+1] and q[i+1][j]==0 and q[i+2][j]==0 and q[i-2][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i+3][j]!=0 and q[i-3][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i+2][j+1]=1
                        q[i+1][j]=Circle(p[i+1][j],10)
                        q[i+1][j].draw(win)      
                        q[i+1][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(12):
            for j in range(12):         
                if white[i+1][j] and white[i+1][j+2] and q[i-1][j]==0 and white[i+2][j+1] and q[i+2][j]==0 and q[i-2][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i+3][j]!=0 and q[i-3][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i][j+1]=1
                        q[i-1][j]=Circle(p[i-1][j],10)
                        q[i-1][j].draw(win)      
                        q[i-1][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(12):
            for j in range(12):         
                if white[i+1][j] and q[i][j+1]==0 and white[i][j+1] and white[i+2][j+1] and q[i+2][j]==0 and q[i-2][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i+3][j]!=0 and q[i-3][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i+1][j+2]=1
                        q[i][j+1]=Circle(p[i][j+1],10)
                        q[i][j+1].draw(win)      
                        q[i][j+1].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(12):
            for j in range(12):         
                if q[i][j-1]==0 and white[i+1][j+2] and white[i][j+1] and white[i+2][j+1] and q[i+2][j]==0 and q[i-2][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i+3][j]!=0 and q[i-3][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i+1][j]=1
                        q[i][j-1]=Circle(p[i][j-1],10)
                        q[i][j-1].draw(win)      
                        q[i][j-1].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        
        for i in range(12):
            for j in range(11):         
                if white[i][j+1] and white[i+2][j+1] and white[i+1][j+2] and q[i][j+2]==0 and q[i-2][j]==0 and q[i+2][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-3][j]!=0 and q[i+3][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i+1][j+3]=1
                        q[i][j+2]=Circle(p[i][j+2],10)
                        q[i][j+2].draw(win)      
                        q[i][j+2].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(12):
            for j in range(11):         
                if white[i][j+1] and white[i+2][j+1] and q[i][j+1]==0 and white[i+1][j+3] and q[i-2][j]==0 and q[i+2][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-3][j]!=0 and q[i+3][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i+1][j+2]=1
                        q[i][j+1]=Circle(p[i][j+1],10)
                        q[i][j+1].draw(win)      
                        q[i][j+1].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(12):
            for j in range(11):         
                if white[i][j+1] and q[i+1][j]==0 and white[i+1][j+2] and white[i+1][j+3] and q[i-2][j]==0 and q[i+2][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-3][j]!=0 and q[i+3][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i+2][j+1]=1
                        q[i+1][j]=Circle(p[i+1][j],10)
                        q[i+1][j].draw(win)      
                        q[i+1][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(12):
            for j in range(11):         
                if q[i-1][j]==0 and white[i+2][j+1] and white[i+1][j+2] and white[i+1][j+3] and q[i-2][j]==0 and q[i+2][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-3][j]!=0 and q[i+3][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i][j+1]=1
                        q[i-1][j]=Circle(p[i-1][j],10)
                        q[i-1][j].draw(win)      
                        q[i-1][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue


        for i in range(12):
            for j in range(13):         
                if white[i][j+1] and white[i+2][j+1] and white[i+1][j] and q[i][j-2]==0 and q[i-2][j]==0 and q[i+2][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i-3][j]!=0 and q[i+3][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i+1][j-1]=1
                        q[i][j-2]=Circle(p[i][j-2],10)
                        q[i][j-2].draw(win)      
                        q[i][j-2].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(12):
            for j in range(13):         
                if white[i][j+1] and white[i+2][j+1] and q[i][j-1]==0 and white[i+1][j-1] and q[i-2][j]==0 and q[i+2][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i-3][j]!=0 and q[i+3][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i+1][j]=1
                        q[i][j-1]=Circle(p[i][j-1],10)
                        q[i][j-1].draw(win)      
                        q[i][j-1].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue
        
        for i in range(12):
            for j in range(13):         
                if white[i][j+1] and q[i+1][j]==0 and white[i+1][j] and white[i+1][j-1] and q[i-2][j]==0 and q[i+2][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i-3][j]!=0 and q[i+3][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i+2][j+1]=1
                        q[i+1][j]=Circle(p[i+1][j],10)
                        q[i+1][j].draw(win)      
                        q[i+1][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(12):
            for j in range(13):         
                if q[i-1][j]==0 and white[i+2][j+1] and white[i+1][j] and white[i+1][j-1] and q[i-2][j]==0 and q[i+2][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i-3][j]!=0 and q[i+3][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i][j+1]=1
                        q[i-1][j]=Circle(p[i-1][j],10)
                        q[i-1][j].draw(win)      
                        q[i-1][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue


        for i in range(11):
            for j in range(12):         
                if white[i+1][j] and white[i+1][j+2] and white[i+2][j+1] and q[i+2][j]==0 and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i+3][j+1]=1
                        q[i+2][j]=Circle(p[i+2][j],10)
                        q[i+2][j].draw(win)      
                        q[i+2][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(12):         
                if white[i+1][j] and white[i+1][j+2] and q[i+1][j]==0 and white[i+3][j+1] and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i+2][j+1]=1
                        q[i+1][j]=Circle(p[i+1][j],10)
                        q[i+1][j].draw(win)      
                        q[i+1][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(12):         
                if white[i+1][j] and q[i][j+1]==0 and white[i+2][j+1] and white[i+3][j+1] and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i+1][j+2]=1
                        q[i][j+1]=Circle(p[i][j+1],10)
                        q[i][j+1].draw(win)      
                        q[i][j+1].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(12):         
                if q[i][j-1]==0 and white[i+1][j+2] and white[i+2][j+1] and white[i+3][j+1] and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i+1][j]=1
                        q[i][j-1]=Circle(p[i][j-1],10)
                        q[i][j-1].draw(win)      
                        q[i][j-1].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        
        for i in range(13):
            for j in range(12):         
                if white[i+1][j] and white[i+1][j+2] and white[i][j+1] and q[i-2][j]==0 and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i+2][j]!=0 and q[i-4][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i-1][j+1]=1
                        q[i-2][j]=Circle(p[i-2][j],10)
                        q[i-2][j].draw(win)      
                        q[i-2][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(13):
            for j in range(12):         
                if white[i+1][j] and white[i+1][j+2] and q[i-1][j]==0 and white[i-1][j+1] and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i+2][j]!=0 and q[i-4][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i-1][j+1]=1
                        q[i-1][j]=Circle(p[i-1][j],10)
                        q[i-1][j].draw(win)      
                        q[i-1][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue    

        for i in range(13):
            for j in range(12):         
                if white[i+1][j] and q[i][j+1]==0 and white[i][j+1] and white[i-1][j+1] and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i+2][j]!=0 and q[i-4][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i+1][j+2]=1
                        q[i][j+1]=Circle(p[i][j+1],10)
                        q[i][j+1].draw(win)      
                        q[i][j+1].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue    

        for i in range(13):
            for j in range(12):         
                if q[i][j-1]==0 and white[i+1][j+2] and white[i][j+1] and white[i-1][j+1] and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i+2][j]!=0 and q[i-4][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i+1][j]=1
                        q[i][j-1]=Circle(p[i][j-1],10)
                        q[i][j-1].draw(win)      
                        q[i][j-1].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue    

        
        for i in range(11):
            for j in range(11):         
                if white[i+3][j+1] and white[i+2][j+1] and white[i+1][j+2] and q[i][j+2]==0 and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i+1][j+3]=1
                        q[i][j+2]=Circle(p[i][j+2],10)
                        q[i][j+2].draw(win)      
                        q[i][j+2].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(11):         
                if white[i+3][j+1] and white[i+2][j+1] and q[i][j+1]==0 and white[i+1][j+3] and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i+1][j+2]=1
                        q[i][j+1]=Circle(p[i][j+1],10)
                        q[i][j+1].draw(win)      
                        q[i][j+1].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(11):         
                if white[i+3][j+1] and q[i+1][j]==0 and white[i+1][j+2] and white[i+1][j+3] and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i+2][j+1]=1
                        q[i+1][j]=Circle(p[i+1][j],10)
                        q[i+1][j].draw(win)      
                        q[i+1][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(11):         
                if q[i+2][j]==0 and white[i+2][j+1] and white[i+1][j+2] and white[i+1][j+3] and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i+3][j+1]=1
                        q[i+2][j]=Circle(p[i+2][j],10)
                        q[i+2][j].draw(win)      
                        q[i+2][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue


        for i in range(11):
            for j in range(13):         
                if white[i+3][j+1] and white[i+2][j+1] and white[i+1][j] and q[i][j-2]==0 and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i+1][j-1]=1
                        q[i][j-2]=Circle(p[i][j-2],10)
                        q[i][j-2].draw(win)      
                        q[i][j-2].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(13):         
                if white[i+3][j+1] and white[i+2][j+1] and q[i][j-1]==0 and white[i+1][j-1] and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i+1][j]=1
                        q[i][j-1]=Circle(p[i][j-1],10)
                        q[i][j-1].draw(win)      
                        q[i][j-1].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(13):         
                if white[i+3][j+1] and q[i+1][j]==0 and white[i+1][j] and white[i+1][j-1] and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i+2][j+1]=1
                        q[i+1][j]=Circle(p[i+1][j],10)
                        q[i+1][j].draw(win)      
                        q[i+1][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(13):         
                if q[i+2][j]==0 and white[i+2][j+1] and white[i+1][j] and white[i+1][j-1] and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i+3][j+1]=1
                        q[i+2][j]=Circle(p[i+2][j],10)
                        q[i+2][j].draw(win)      
                        q[i+2][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue


        for i in range(13):
            for j in range(11):         
                if white[i+1][j+3] and white[i+1][j+2] and white[i][j+1] and q[i-2][j]==0 and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-4][j]!=0 and q[i+2][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i-1][j+1]=1
                        q[i-2][j]=Circle(p[i-2][j],10)
                        q[i-2][j].draw(win)      
                        q[i-2][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(13):
            for j in range(11):         
                if white[i+1][j+3] and white[i+1][j+2] and q[i-1][j]==0 and white[i-1][j+1] and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-4][j]!=0 and q[i+2][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i][j+1]=1
                        q[i-1][j]=Circle(p[i-1][j],10)
                        q[i-1][j].draw(win)      
                        q[i-1][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(13):
            for j in range(11):         
                if white[i+1][j+3] and q[i][j+1]==0 and white[i][j+1] and white[i-1][j+1] and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-4][j]!=0 and q[i+2][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i+1][j+2]=1
                        q[i][j+1]=Circle(p[i][j+1],10)
                        q[i][j+1].draw(win)      
                        q[i][j+1].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(13):
            for j in range(11):         
                if q[i][j+2]==0 and white[i+1][j+2] and white[i][j+1] and white[i-1][j+1] and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-4][j]!=0 and q[i+2][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i+1][j+3]=1
                        q[i][j+2]=Circle(p[i][j+2],10)
                        q[i][j+2].draw(win)      
                        q[i][j+2].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue


        for i in range(13):
            for j in range(13):         
                if white[i+1][j] and white[i+1][j-1] and white[i][j+1] and q[i-2][j]==0 and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i+2][j]!=0 and q[i-4][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i-1][j+1]=1
                        q[i-2][j]=Circle(p[i-2][j],10)
                        q[i-2][j].draw(win)      
                        q[i-2][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(13):
            for j in range(13):         
                if white[i+1][j] and white[i+1][j-1] and q[i-1][j]==0 and white[i-1][j+1] and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i+2][j]!=0 and q[i-4][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i][j+1]=1
                        q[i-1][j]=Circle(p[i-1][j],10)
                        q[i-1][j].draw(win)      
                        q[i-1][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(13):
            for j in range(13):         
                if white[i+1][j] and q[i][j-2]==0 and white[i][j+1] and white[i-1][j+1] and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i+2][j]!=0 and q[i-4][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i+1][j-1]=1
                        q[i][j-2]=Circle(p[i][j-2],10)
                        q[i][j-2].draw(win)      
                        q[i][j-2].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(13):
            for j in range(13):         
                if q[i][j-1]==0 and white[i+1][j-1] and white[i][j+1] and white[i-1][j+1] and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i+2][j]!=0 and q[i-4][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i+1][j]=1
                        q[i][j-1]=Circle(p[i][j-1],10)
                        q[i][j-1].draw(win)      
                        q[i][j-1].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue


        for i in range(12):
            for j in range(12):         
                if black[i+1][j] and black[i+1][j+2] and black[i][j+1] and q[i+1][j]==0 and q[i+2][j]==0 and q[i-2][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i+3][j]!=0 and q[i-3][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i+2][j+1]=1
                        q[i+1][j]=Circle(p[i+1][j],10)
                        q[i+1][j].draw(win)      
                        q[i+1][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(12):
            for j in range(12):         
                if black[i+1][j] and black[i+1][j+2] and q[i-1][j]==0 and black[i+2][j+1] and q[i+2][j]==0 and q[i-2][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i+3][j]!=0 and q[i-3][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i][j+1]=1
                        q[i-1][j]=Circle(p[i-1][j],10)
                        q[i-1][j].draw(win)      
                        q[i-1][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(12):
            for j in range(12):         
                if black[i+1][j] and q[i][j+1]==0 and black[i][j+1] and black[i+2][j+1] and q[i+2][j]==0 and q[i-2][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i+3][j]!=0 and q[i-3][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i+1][j+2]=1
                        q[i][j+1]=Circle(p[i][j+1],10)
                        q[i][j+1].draw(win)      
                        q[i][j+1].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(12):
            for j in range(12):         
                if q[i][j-1]==0 and black[i+1][j+2] and black[i][j+1] and black[i+2][j+1] and q[i+2][j]==0 and q[i-2][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i+3][j]!=0 and q[i-3][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i+1][j]=1
                        q[i][j-1]=Circle(p[i][j-1],10)
                        q[i][j-1].draw(win)      
                        q[i][j-1].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue


        for i in range(12):
            for j in range(11):         
                if black[i][j+1] and black[i+2][j+1] and black[i+1][j+2] and q[i][j+2]==0 and q[i-2][j]==0 and q[i+2][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-3][j]!=0 and q[i+3][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i+1][j+3]=1
                        q[i][j+2]=Circle(p[i][j+2],10)
                        q[i][j+2].draw(win)      
                        q[i][j+2].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(12):
            for j in range(11):         
                if black[i][j+1] and black[i+2][j+1] and q[i][j+1]==0 and black[i+1][j+3] and q[i-2][j]==0 and q[i+2][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-3][j]!=0 and q[i+3][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i+1][j+2]=1
                        q[i][j+1]=Circle(p[i][j+1],10)
                        q[i][j+1].draw(win)      
                        q[i][j+1].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(12):
            for j in range(11):         
                if black[i][j+1] and q[i+1][j]==0 and black[i+1][j+2] and black[i+1][j+3] and q[i-2][j]==0 and q[i+2][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-3][j]!=0 and q[i+3][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i+2][j+1]=1
                        q[i+1][j]=Circle(p[i+1][j],10)
                        q[i+1][j].draw(win)      
                        q[i+1][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(12):
            for j in range(11):         
                if q[i-1][j]==0 and black[i+2][j+1] and black[i+1][j+2] and black[i+1][j+3] and q[i-2][j]==0 and q[i+2][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-3][j]!=0 and q[i+3][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i][j+1]=1
                        q[i-1][j]=Circle(p[i-1][j],10)
                        q[i-1][j].draw(win)      
                        q[i-1][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue


        for i in range(12):
            for j in range(13):         
                if black[i][j+1] and black[i+2][j+1] and black[i+1][j] and q[i][j-2]==0 and q[i-2][j]==0 and q[i+2][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i-3][j]!=0 and q[i+3][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i+1][j-1]=1
                        q[i][j-2]=Circle(p[i][j-2],10)
                        q[i][j-2].draw(win)      
                        q[i][j-2].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(12):
            for j in range(13):         
                if black[i][j+1] and black[i+2][j+1] and q[i][j-1]==0 and black[i+1][j-1] and q[i-2][j]==0 and q[i+2][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i-3][j]!=0 and q[i+3][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i+1][j]=1
                        q[i][j-1]=Circle(p[i][j-1],10)
                        q[i][j-1].draw(win)      
                        q[i][j-1].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue
        
        for i in range(12):
            for j in range(13):         
                if black[i][j+1] and q[i+1][j]==0 and black[i+1][j] and black[i+1][j-1] and q[i-2][j]==0 and q[i+2][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i-3][j]!=0 and q[i+3][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i+2][j+1]=1
                        q[i+1][j]=Circle(p[i+1][j],10)
                        q[i+1][j].draw(win)      
                        q[i+1][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(12):
            for j in range(13):         
                if q[i-1][j]==0 and black[i+2][j+1] and black[i+1][j] and black[i+1][j-1] and q[i-2][j]==0 and q[i+2][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i-3][j]!=0 and q[i+3][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i][j+1]=1
                        q[i-1][j]=Circle(p[i-1][j],10)
                        q[i-1][j].draw(win)      
                        q[i-1][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue


        for i in range(11):
            for j in range(12):         
                if black[i+1][j] and black[i+1][j+2] and black[i+2][j+1] and q[i+2][j]==0 and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i+3][j+1]=1
                        q[i+2][j]=Circle(p[i+2][j],10)
                        q[i+2][j].draw(win)      
                        q[i+2][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(12):         
                if black[i+1][j] and black[i+1][j+2] and q[i+1][j]==0 and black[i+3][j+1] and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i+2][j+1]=1
                        q[i+1][j]=Circle(p[i+1][j],10)
                        q[i+1][j].draw(win)      
                        q[i+1][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(12):         
                if black[i+1][j] and q[i][j+1]==0 and black[i+2][j+1] and black[i+3][j+1] and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i+1][j+2]=1
                        q[i][j+1]=Circle(p[i][j+1],10)
                        q[i][j+1].draw(win)      
                        q[i][j+1].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(12):         
                if q[i][j-1]==0 and black[i+1][j+2] and black[i+2][j+1] and black[i+3][j+1] and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i+1][j]=1
                        q[i][j-1]=Circle(p[i][j-1],10)
                        q[i][j-1].draw(win)      
                        q[i][j-1].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        
        for i in range(13):
            for j in range(12):         
                if black[i+1][j] and black[i+1][j+2] and black[i][j+1] and q[i-2][j]==0 and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i+2][j]!=0 and q[i-4][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i-1][j+1]=1
                        q[i-2][j]=Circle(p[i-2][j],10)
                        q[i-2][j].draw(win)      
                        q[i-2][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(13):
            for j in range(12):         
                if black[i+1][j] and black[i+1][j+2] and q[i-1][j]==0 and black[i-1][j+1] and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i+2][j]!=0 and q[i-4][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i-1][j+1]=1
                        q[i-1][j]=Circle(p[i-1][j],10)
                        q[i-1][j].draw(win)      
                        q[i-1][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue    

        for i in range(13):
            for j in range(12):         
                if black[i+1][j] and q[i][j+1]==0 and black[i][j+1] and black[i-1][j+1] and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i+2][j]!=0 and q[i-4][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i+1][j+2]=1
                        q[i][j+1]=Circle(p[i][j+1],10)
                        q[i][j+1].draw(win)      
                        q[i][j+1].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue    

        for i in range(13):
            for j in range(12):         
                if q[i][j-1]==0 and black[i+1][j+2] and black[i][j+1] and black[i-1][j+1] and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+2]==0 and q[i][j-2]==0 and q[i][j]==0:
                    if q[i+2][j]!=0 and q[i-4][j]!=0:
                        break
                    elif q[i][j-3]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i+1][j]=1
                        q[i][j-1]=Circle(p[i][j-1],10)
                        q[i][j-1].draw(win)      
                        q[i][j-1].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue    

        
        for i in range(11):
            for j in range(11):         
                if black[i+3][j+1] and black[i+2][j+1] and black[i+1][j+2] and q[i][j+2]==0 and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i+1][j+3]=1
                        q[i][j+2]=Circle(p[i][j+2],10)
                        q[i][j+2].draw(win)      
                        q[i][j+2].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(11):         
                if black[i+3][j+1] and black[i+2][j+1] and q[i][j+1]==0 and black[i+1][j+3] and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i+1][j+2]=1
                        q[i][j+1]=Circle(p[i][j+1],10)
                        q[i][j+1].draw(win)      
                        q[i][j+1].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(11):         
                if black[i+3][j+1] and q[i+1][j]==0 and black[i+1][j+2] and black[i+1][j+3] and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i+2][j+1]=1
                        q[i+1][j]=Circle(p[i+1][j],10)
                        q[i+1][j].draw(win)      
                        q[i+1][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(11):         
                if q[i+2][j]==0 and black[i+2][j+1] and black[i+1][j+2] and black[i+1][j+3] and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i+3][j+1]=1
                        q[i+2][j]=Circle(p[i+2][j],10)
                        q[i+2][j].draw(win)      
                        q[i+2][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue


        for i in range(11):
            for j in range(13):         
                if black[i+3][j+1] and black[i+2][j+1] and black[i+1][j] and q[i][j-2]==0 and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i+1][j-1]=1
                        q[i][j-2]=Circle(p[i][j-2],10)
                        q[i][j-2].draw(win)      
                        q[i][j-2].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(13):         
                if black[i+3][j+1] and black[i+2][j+1] and q[i][j-1]==0 and black[i+1][j-1] and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i+1][j]=1
                        q[i][j-1]=Circle(p[i][j-1],10)
                        q[i][j-1].draw(win)      
                        q[i][j-1].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(13):         
                if black[i+3][j+1] and q[i+1][j]==0 and black[i+1][j] and black[i+1][j-1] and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i+2][j+1]=1
                        q[i+1][j]=Circle(p[i+1][j],10)
                        q[i+1][j].draw(win)      
                        q[i+1][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(13):         
                if q[i+2][j]==0 and black[i+2][j+1] and black[i+1][j] and black[i+1][j-1] and q[i-1][j]==0 and q[i+3][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i-2][j]!=0 and q[i+4][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i+3][j+1]=1
                        q[i+2][j]=Circle(p[i+2][j],10)
                        q[i+2][j].draw(win)      
                        q[i+2][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue


        for i in range(13):
            for j in range(11):         
                if black[i+1][j+3] and black[i+1][j+2] and black[i][j+1] and q[i-2][j]==0 and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-4][j]!=0 and q[i+2][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i-1][j+1]=1
                        q[i-2][j]=Circle(p[i-2][j],10)
                        q[i-2][j].draw(win)      
                        q[i-2][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(13):
            for j in range(11):         
                if black[i+1][j+3] and black[i+1][j+2] and q[i-1][j]==0 and black[i-1][j+1] and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-4][j]!=0 and q[i+2][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i][j+1]=1
                        q[i-1][j]=Circle(p[i-1][j],10)
                        q[i-1][j].draw(win)      
                        q[i-1][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(13):
            for j in range(11):         
                if black[i+1][j+3] and q[i][j+1]==0 and black[i][j+1] and black[i-1][j+1] and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-4][j]!=0 and q[i+2][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i+1][j+2]=1
                        q[i][j+1]=Circle(p[i][j+1],10)
                        q[i][j+1].draw(win)      
                        q[i][j+1].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(13):
            for j in range(11):         
                if q[i][j+2]==0 and black[i+1][j+2] and black[i][j+1] and black[i-1][j+1] and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+3]==0 and q[i][j-1]==0 and q[i][j]==0:
                    if q[i-4][j]!=0 and q[i+2][j]!=0:
                        break
                    elif q[i][j-2]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i+1][j+3]=1
                        q[i][j+2]=Circle(p[i][j+2],10)
                        q[i][j+2].draw(win)      
                        q[i][j+2].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue


        for i in range(13):
            for j in range(13):         
                if black[i+1][j] and black[i+1][j-1] and black[i][j+1] and q[i-2][j]==0 and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i+2][j]!=0 and q[i-4][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i-1][j+1]=1
                        q[i-2][j]=Circle(p[i-2][j],10)
                        q[i-2][j].draw(win)      
                        q[i-2][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(13):
            for j in range(13):         
                if black[i+1][j] and black[i+1][j-1] and q[i-1][j]==0 and black[i-1][j+1] and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i+2][j]!=0 and q[i-4][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i][j+1]=1
                        q[i-1][j]=Circle(p[i-1][j],10)
                        q[i-1][j].draw(win)      
                        q[i-1][j].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(13):
            for j in range(13):         
                if black[i+1][j] and q[i][j-2]==0 and black[i][j+1] and black[i-1][j+1] and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i+2][j]!=0 and q[i-4][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i+1][j-1]=1
                        q[i][j-2]=Circle(p[i][j-2],10)
                        q[i][j-2].draw(win)      
                        q[i][j-2].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(13):
            for j in range(13):         
                if q[i][j-1]==0 and black[i+1][j-1] and black[i][j+1] and black[i-1][j+1] and q[i+1][j]==0 and q[i-3][j]==0 and q[i][j+1]==0 and q[i][j-3]==0 and q[i][j]==0:
                    if q[i+2][j]!=0 and q[i-4][j]!=0:
                        break
                    elif q[i][j-4]!=0 and q[i][j+2]!=0:
                        break
                    else:
                        black[i+1][j]=1
                        q[i][j-1]=Circle(p[i][j-1],10)
                        q[i][j-1].draw(win)      
                        q[i][j-1].setFill('black')
                        n=1
                        break
            if n==1:
                break
        if n==1:
            n=0
            continue
                                            
        for i in range(15):
            for j in range(9):    
                if black[i+1][j+3] and black[i+1][j+5] and q[i][j]==0 and q[i][j+1]==0 and q[i][j+5]==0 and q[i][j+6]==0 and q[i][j+3]==0:
                    black[i+1][j+4]=1
                    q[i][j+3]=Circle(p[i][j+3],10)
                    q[i][j+3].draw(win)      
                    q[i][j+3].setFill('black')
                    n=1
                    break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(15):
            for j in range(10):    
                if black[i+1][j+3] and black[i+1][j+4] and q[i][j]==0 and q[i][j+1]==0 and q[i][j+5]==0 and q[i][j+4]==0:
                    black[i+1][j+5]=1
                    q[i][j+4]=Circle(p[i][j+4],10)
                    q[i][j+4].draw(win)      
                    q[i][j+4].setFill('black')
                    n=1
                    break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(9):
            for j in range(15):    
                if black[i+3][j+1] and black[i+5][j+1] and q[i][j]==0 and q[i+1][j]==0 and q[i+5][j]==0 and q[i+6][j]==0 and q[i+3][j]==0:
                    black[i+4][j+1]=1
                    q[i+3][j]=Circle(p[i+3][j],10)
                    q[i+3][j].draw(win)      
                    q[i+3][j].setFill('black')
                    n=1
                    break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(10):
            for j in range(15):    
                if black[i+3][j+1] and black[i+4][j+1] and q[i][j]==0 and q[i+1][j]==0 and q[i+5][j]==0 and q[i+4][j]==0:
                    black[i+5][j+1]=1
                    q[i+4][j]=Circle(p[i+4][j],10)
                    q[i+4][j].draw(win)      
                    q[i+4][j].setFill('black')
                    n=1
                    break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(9):
            for j in range(9):    
                if black[i+3][j+3] and black[i+5][j+5] and q[i][j]==0 and q[i+1][j+1]==0 and q[i+5][j+5]==0 and q[i+6][j+6]==0 and q[i+3][j+3]==0:
                    black[i+4][j+4]=1
                    q[i+3][j+3]=Circle(p[i+3][j+3],10)
                    q[i+3][j+3].draw(win)      
                    q[i+3][j+3].setFill('black')
                    n=1
                    break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(10):
            for j in range(10):    
                if black[i+3][j+3] and black[i+4][j+4] and q[i][j]==0 and q[i+1][j+1]==0 and q[i+5][j+5]==0 and q[i+4][j+4]==0:
                    black[i+5][j+5]=1
                    q[i+4][j+4]=Circle(p[i+4][j+4],10)
                    q[i+4][j+4].draw(win)      
                    q[i+4][j+4].setFill('black')
                    n=1
                    break
            if n==1:
                break
        if n==1:
            n=0
            continue
        
        for i in range(9):
            for j in range(15):    
                if black[i+3][j-1] and black[i+5][j-3] and q[i][j]==0 and q[i+1][j-1]==0 and q[i+5][j-5]==0 and q[i+6][j-6]==0 and q[i+3][j-3]==0:
                    black[i+4][j-2]=1
                    q[i+3][j-3]=Circle(p[i+3][j-3],10)
                    q[i+3][j-3].draw(win)      
                    q[i+3][j-3].setFill('black')
                    n=1
                    break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(10):
            for j in range(15):    
                if black[i+3][j-1] and black[i+4][j-2] and q[i][j]==0 and q[i+1][j-1]==0 and q[i+5][j-5]==0 and q[i+4][j-4]==0:
                    black[i+5][j-3]=1
                    q[i+4][j-4]=Circle(p[i+4][j-4],10)
                    q[i+4][j-4].draw(win)      
                    q[i+4][j-4].setFill('black')
                    n=1
                    break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(15):
            for j in range(11):    
                if black[i+1][j+1] and black[i+1][j+2] and black[i+1][j+3] and q[i][j+3]==0:
                    if q[i][j-1]!=0 and q[i][j+4]!=0:
                        break
                    else:
                        black[i+1][j+4]=1
                        q[i][j+3]=Circle(p[i][j+3],10)
                        q[i][j+3].draw(win)      
                        q[i][j+3].setFill('black')
                        n=1
                        break
                elif black[i+1][j+1] and black[i+1][j+2] and black[i+1][j+3] and q[i][j-1]==0:
                    if q[i][j-2]!=0 and q[i][j+3]!=0:
                        break
                    else:
                        black[i+1][j]=1
                        q[i][j-1]=Circle(p[i][j-1],10)
                        q[i][j-1].draw(win)      
                        q[i][j-1].setFill('black')
                        n=1
                        break
            if n==1:    
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(15):              
                if black[i+1][j+1] and black[i+2][j+1]and black[i+3][j+1] and q[i+3][j]==0:
                    if q[i-1][j]!=0 and q[i+4][j]!=0:
                        break
                    else:
                        black[i+4][j+1]=1
                        q[i+3][j]=Circle(p[i+3][j],10)
                        q[i+3][j].draw(win)      
                        q[i+3][j].setFill('black')
                        n=1
                        break
                elif black[i+1][j+1] and black[i+2][j+1] and black[i+3][j+1] and q[i-1][j]==0:
                    if q[i-2][j]!=0 and q[i+3][j]!=0:
                        break
                    else:
                        black[i][j+1]=1
                        q[i-1][j]=Circle(p[i-1][j],10)
                        q[i-1][j].draw(win)      
                        q[i-1][j].setFill('black')
                        n=1
                        break
            if n==1:    
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(11):    
                if black[i+1][j+1] and black[i+2][j+2]and black[i+3][j+3] and q[i+3][j+3]==0:
                    if q[i-1][j-1]!=0 and q[i+4][j+4]!=0:
                        break
                    else:
                        black[i+4][j+4]=1
                        q[i+3][j+3]=Circle(p[i+3][j+3],10)
                        q[i+3][j+3].draw(win)      
                        q[i+3][j+3].setFill('black')
                        n=1
                        break
                elif black[i+1][j+1] and black[i+2][j+2]and black[i+3][j+3] and q[i-1][j-1]==0:
                    if q[i-2][j-2]!=0 and q[i+3][j+3]!=0:
                        break
                    else:
                        black[i][j]=1
                        q[i-1][j-1]=Circle(p[i-1][j-1],10)
                        q[i-1][j-1].draw(win)      
                        q[i-1][j-1].setFill('black')
                        n=1
                        break              
            if n==1:    
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(15):    
                if black[i+1][j+1] and black[i+2][j]and black[i+3][j-1]and q[i+3][j-3]==0:
                    if q[i-1][j+1]!=0 and q[i+4][j-4]!=0:
                        break
                    else:
                        black[i+4][j-2]=1
                        q[i+3][j-3]=Circle(p[i+3][j-3],10)
                        q[i+3][j-3].draw(win)      
                        q[i+3][j-3].setFill('black')
                        n=1
                        break
                elif black[i+1][j+1] and black[i+2][j]and black[i+3][j-1]and q[i-1][j+1]==0:
                    if q[i-2][j+2]!=0 and q[i+3][j-3]!=0:
                        break
                    else:
                        black[i][j+2]=1
                        q[i-1][j+1]=Circle(p[i-1][j+1],10)
                        q[i-1][j+1].draw(win)      
                        q[i-1][j+1].setFill('black')
                        n=1
                        break
            if n==1:    
                break
        if n==1:
            n=0
            continue

        

        for i in range(15):
            for j in range(10):    
                if black[i+1][j+3] and black[i+1][j+5] and q[i][j+1]==0 and q[i][j+5]==0 and q[i][j+3]==0:
                    black[i+1][j+4]=1
                    q[i][j+3]=Circle(p[i][j+3],10)
                    q[i][j+3].draw(win)      
                    q[i][j+3].setFill('black')
                    n=1
                    break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(15):
            for j in range(11):    
                if black[i+1][j+3] and black[i+1][j+4] and q[i][j+1]==0 and q[i][j+4]==0:
                    black[i+1][j+5]=1
                    q[i][j+4]=Circle(p[i][j+4],10)
                    q[i][j+4].draw(win)      
                    q[i][j+4].setFill('black')
                    n=1
                    break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(10):
            for j in range(15):    
                if black[i+3][j+1] and black[i+5][j+1] and q[i+1][j]==0 and q[i+5][j]==0 and q[i+3][j]==0:
                    black[i+4][j+1]=1
                    q[i+3][j]=Circle(p[i+3][j],10)
                    q[i+3][j].draw(win)      
                    q[i+3][j].setFill('black')
                    n=1
                    break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(15):    
                if black[i+3][j+1] and black[i+4][j+1] and q[i+1][j]==0 and q[i+4][j]==0:
                    black[i+5][j+1]=1
                    q[i+4][j]=Circle(p[i+4][j],10)
                    q[i+4][j].draw(win)      
                    q[i+4][j].setFill('black')
                    n=1
                    break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(10):
            for j in range(10):    
                if black[i+3][j+3] and black[i+5][j+5] and q[i+1][j+1]==0 and q[i+5][j+5]==0 and q[i+3][j+3]==0:
                    black[i+4][j+4]=1
                    q[i+3][j+3]=Circle(p[i+3][j+3],10)
                    q[i+3][j+3].draw(win)      
                    q[i+3][j+3].setFill('black')
                    n=1
                    break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(11):    
                if black[i+3][j+3] and black[i+4][j+4] and q[i+1][j+1]==0 and q[i+4][j+4]==0:
                    black[i+5][j+5]=1
                    q[i+4][j+4]=Circle(p[i+4][j+4],10)
                    q[i+4][j+4].draw(win)      
                    q[i+4][j+4].setFill('black')
                    n=1
                    break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(10):
            for j in range(15):    
                if black[i+3][j-1] and black[i+5][j-3] and q[i+1][j-1]==0 and q[i+5][j-5]==0 and q[i+3][j-3]==0:
                    black[i+4][j-2]=1
                    q[i+3][j-3]=Circle(p[i+3][j-3],10)
                    q[i+3][j-3].draw(win)      
                    q[i+3][j-3].setFill('black')
                    n=1
                    break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(11):
            for j in range(15):    
                if black[i+3][j-1] and black[i+4][j-2] and q[i+1][j-1]==0 and q[i+4][j-4]==0:
                    black[i+5][j-3]=1
                    q[i+4][j-4]=Circle(p[i+4][j-4],10)
                    q[i+4][j-4].draw(win)      
                    q[i+4][j-4].setFill('black')
                    n=1
                    break
            if n==1:
                break
        if n==1:
            n=0
            continue
    
        for i in range(13):
            for j in range(13): 
                if q[i-2][j-2]==0 and q[i-1][j-1]==0 and q[i+1][j+1]==0 and q[i+2][j+2]==0 and black[i+1][j+1]:
                    black[i+2][j+2]=1
                    q[i+1][j+1]=Circle(p[i+1][j+1],10)
                    q[i+1][j+1].draw(win)      
                    q[i+1][j+1].setFill('black')
                    n=1
                    break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(13):
            for j in range(13): 
                if q[i-2][j+2]==0 and q[i-1][j+1]==0 and q[i+1][j-1]==0 and q[i+2][j-2]==0 and black[i+1][j+1]:
                    black[i+2][j]=1
                    q[i+1][j-1]=Circle(p[i+1][j-1],10)
                    q[i+1][j-1].draw(win)      
                    q[i+1][j-1].setFill('black')
                    n=1
                    break
            if n==1:
                break
        if n==1:
            n=0
            continue
    
        for i in range(14):
            for j in range(13): 
                if q[i][j-2]==0 and q[i][j-1]==0 and q[i][j+1]==0 and q[i][j+2]==0 and black[i+1][j+1]:
                    black[i+1][j+2]=1
                    q[i][j+1]=Circle(p[i][j+1],10)
                    q[i][j+1].draw(win)      
                    q[i][j+1].setFill('black')
                    n=1
                    break
            if n==1:
                break
        if n==1:
            n=0
            continue
    
        for i in range(13):
            for j in range(14): 
                if q[i-2][j]==0 and q[i-1][j]==0 and q[i+1][j]==0 and q[i+2][j]==0 and black[i+1][j+1]:
                    black[i+2][j+1]=1
                    q[i+1][j]=Circle(p[i+1][j],10)
                    q[i+1][j].draw(win)      
                    q[i+1][j].setFill('black')
                    n=1
                    break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(14):
            for j in range(14): 
                if q[i-2][j-2]==0 and q[i-1][j-1]==0 and q[i+1][j+1]==0 and black[i+1][j+1]:
                    black[i+2][j+2]=1
                    q[i+1][j+1]=Circle(p[i+1][j+1],10)
                    q[i+1][j+1].draw(win)      
                    q[i+1][j+1].setFill('black')
                    n=1
                    break
            if n==1:
                break
        if n==1:
            n=0
            continue
        
        for i in range(14):
            for j in range(13): 
                if q[i-2][j+2]==0 and q[i-1][j+1]==0 and q[i+1][j-1]==0 and black[i+1][j+1]:
                    black[i+2][j]=1
                    q[i+1][j-1]=Circle(p[i+1][j-1],10)
                    q[i+1][j-1].draw(win)      
                    q[i+1][j-1].setFill('black')
                    n=1
                    break
            if n==1:
                break
        if n==1:
            n=0
            continue

        for i in range(15):
            for j in range(14): 
                if q[i][j-2]==0 and q[i][j-1]==0 and q[i][j+1]==0 and black[i+1][j+1]:
                    black[i+1][j+2]=1
                    q[i][j+1]=Circle(p[i][j+1],10)
                    q[i][j+1].draw(win)      
                    q[i][j+1].setFill('black')
                    n=1
                    break
            if n==1:
                break
        if n==1:
            n=0
            continue
        
        for i in range(14):
            for j in range(15): 
                if q[i-2][j]==0 and q[i-1][j]==0 and q[i+1][j]==0 and black[i+1][j+1]:
                    black[i+2][j+1]=1
                    q[i+1][j]=Circle(p[i+1][j],10)
                    q[i+1][j].draw(win)      
                    q[i+1][j].setFill('black')
                    n=1
                    break
            if n==1:
                break
        if n==1:
            n=0
            continue

        k=np.random.randint(1,5)
        if q[x1+1][y1+1]==0 and k==1:
            black[x1+2][y1+2]=1
            q[x1+1][y1+1]=Circle(p[x1+1][y1+1],10)
            q[x1+1][y1+1].draw(win)      
            q[x1+1][y1+1].setFill('black')
            n=1
        elif q[x1-1][y1-1]==0 and k==2:
            black[x1][y1]=1
            q[x1-1][y1-1]=Circle(p[x1-1][y1-1],10)
            q[x1-1][y1-1].draw(win)      
            q[x1-1][y1-1].setFill('black')
            n=1
        elif q[x1+1][y1-1]==0 and k==3:
            black[x1+2][y1]=1
            q[x1+1][y1-1]=Circle(p[x1+1][y1-1],10)
            q[x1+1][y1-1].draw(win)      
            q[x1+1][y1-1].setFill('black')
            n=1
        elif q[x1-1][y1+1]==0 and k==4:
            black[x1][y1+2]=1
            q[x1-1][y1+1]=Circle(p[x1-1][y1+1],10)
            q[x1-1][y1+1].draw(win)      
            q[x1-1][y1+1].setFill('black')
            n=1
        if n==1:
            n=0
            continue
        
        t=np.random.randint(1,5)
        if q[x1+1][y1]==0 and t==1:
            black[x1+2][y1+1]=1
            q[x1+1][y1]=Circle(p[x1+1][y1],10)
            q[x1+1][y1].draw(win)      
            q[x1+1][y1].setFill('black')
        elif q[x1-1][y1]==0 and t==2:
            black[x1][y1+1]=1
            q[x1-1][y1]=Circle(p[x1-1][y1],10)
            q[x1-1][y1].draw(win)      
            q[x1-1][y1].setFill('black')
        elif q[x1][y1+1]==0 and t==3:
            black[x1+1][y1+2]=1
            q[x1][y1+1]=Circle(p[x1][y1+1],10)
            q[x1][y1+1].draw(win)      
            q[x1][y1+1].setFill('black')
        elif q[x1][y1-1]==0 and t==4:
            black[x1+1][y1]=1
            q[x1][y1-1]=Circle(p[x1][y1-1],10)
            q[x1][y1-1].draw(win)      
            q[x1][y1-1].setFill('black')          
    win.getMouse()
main()